<html>
<head>
    <title>Группа ${group.getName()}</title>
</head>
<body>
${group.getId()}<br>
${group.getName()}<br>
<#list group.getStudents()>
<p>Groups:
<ul>
    <#items as student>
        <li>${student.getName()}</li>
    </#items>
</ul>
<#else>
<p>This group has no students.
</#list>
</body>
</html>