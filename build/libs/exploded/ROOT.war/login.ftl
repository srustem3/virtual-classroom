<html>
<head>
    <title>Вход</title>
</head>
<body>
<#if message??><h1>${message}</h1></#if>
<form action="/login" method="post">
    <label for="login">Login or email</label>
    <input type="text" id="login" name="login" required><br>
    <label for="password">Password</label>
    <input type="password" id="password" name="password" required><br>
    <label for="rememberMe">Remember me</label>
    <input type="checkbox" id="rememberMe" name="rememberMe" value="rememberMe"><br>
    <input type="submit" value="Войти">
</form>
</body>
</html>