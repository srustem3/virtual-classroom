<#list groups>
<p>Groups:
<ul>
    <#items as group>
        <li>${group.getName()}</li>
    </#items>
</ul>
<#else>
    <p>You have no groups where you are a teacher.
</#list>