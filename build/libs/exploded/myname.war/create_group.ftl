<html>
<head>
    <title>Создание группы</title>
</head>
<body>
<#if message??><h1>${message}</h1></#if>
<form action="/create_group" method="post">
    <label for="name">Название группы</label>
    <input type="text" id="name" name="name" required><br>
    <input type="submit" value="Создать группу">
</form>
</body>
</html>