<html>
<head>
    <title>Регистрация</title>
</head>
<body>
    <#if error??><h1>${error}</h1></#if>
    <form action="/register" method="post">
        <label for="email">Email</label>
        <input type="email" id="email" name="email" required><br>
        <label for="login">Login</label>
        <input type="text" id="login" name="login" required><br>
        <label for="password">Password</label>
        <input type="password" id="password" name="password" required><br>
        <label for="name">Name</label>
        <input type="text" id="name" name="name" required><br>
        <label for="surname">Surname</label>
        <input type="text" id="surname" name="surname" required><br>
        <input type="submit" value="Зарегистрироваться">
    </form>
</body>
</html>