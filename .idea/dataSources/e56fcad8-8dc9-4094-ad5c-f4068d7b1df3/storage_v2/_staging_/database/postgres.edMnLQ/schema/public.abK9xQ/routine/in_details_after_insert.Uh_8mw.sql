CREATE FUNCTION in_details_after_insert()
  RETURNS trigger
LANGUAGE plpgsql
AS $$
BEGIN
  update supplier set total_in_details=total_in_details+new.number where supplier.id=new.supplier_id;
  if (exists(select * from number_of_details where new.sklad_id=number_of_details.sklad_id and new.detail_id=number_of_details.detail_id)) then
    update number_of_details set number_of_details.number=number_of_details.number+new.number where new.sklad_id=number_of_details.sklad_id and new.detail_id=number_of_details.detail_id;
  else
    insert into number_of_details (detail_id, sklad_id, number) values (new.detail_id, new.sklad_id, new.number);
  end if;
  RETURN new;
END;
$$;

