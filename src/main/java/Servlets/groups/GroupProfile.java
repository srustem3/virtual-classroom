package Servlets.groups;

import Entities.Group;
import Entities.StudentGroup;
import Entities.User;
import Repository.AndSpec;
import Repository.TrueSpec;
import Repository.group.GroupSpecById;
import Repository.group.GroupsJpqlRepository;
import Repository.studentGroup.StudentGroupJpqlRepository;
import Repository.studentGroup.StudentGroupSpecByGroup;
import Repository.studentGroup.StudentGroupSpecByStudent;
import Repository.user.UserSourceFromGroup;
import Repository.user.UserSpecById;
import Repository.user.UsersJpqlRepository;
import checks.CheckAndProcessRussianParameter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "GroupProfile", urlPatterns = "/groups/*")
public class GroupProfile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Pattern pattern = Pattern.compile("/(\\d+)");
        Matcher matcher = pattern.matcher(request.getPathInfo());
        if (matcher.find()) {
            long groupId = Long.parseLong(matcher.group(1));
            List<Group> groupList = GroupsJpqlRepository.getRepository().query(new GroupSpecById(groupId));
            if (groupList.size() == 1) {
                Group group = groupList.get(0);
                User teacher = (User) request.getAttribute("user");
                if (group.getTeacher().getId() == teacher.getId()) {
                    if (request.getParameter("delete") == null) {
                        String studentIdToDelete = request.getParameter("delete_student");
                        if (studentIdToDelete == null) {
                            List<User> studentList = UsersJpqlRepository.getRepository().query(new UserSourceFromGroup(group), new TrueSpec<>());
                            request.setAttribute("group", group);
                            request.setAttribute("studentList", studentList);
                            CheckAndProcessRussianParameter.check(request);
                            request.getRequestDispatcher("/templates/group_profile.ftl").forward(request, response);
                        } else {
                            List<User> studentList = UsersJpqlRepository
                                    .getRepository()
                                    .query(new UserSpecById(Long.parseLong(studentIdToDelete)));
                            if (studentList.size() == 1) {
                                User student = studentList.get(0);
                                List<StudentGroup> studentGroupList = StudentGroupJpqlRepository
                                        .getRepository()
                                        .query(new AndSpec<>(new StudentGroupSpecByStudent(student),
                                                new StudentGroupSpecByGroup(group)));
                                if (studentGroupList.size() == 1) {
                                    StudentGroupJpqlRepository.getRepository().remove(studentGroupList.get(0));
                                    response.sendRedirect("/groups/" + groupId);
                                } else {
                                    response.sendRedirect(String.format("/groups/%s?message=%s", groupId, "You don't have this student in this group"));
                                }
                            } else {
                                response.sendRedirect(String.format("/groups/%s?message=%s", groupId, "This student does not exist"));
                            }
                        }
                    } else {
                        GroupsJpqlRepository.getRepository().remove(group);
                        response.sendRedirect("/groups");
                    }
                } else {
                    response.getWriter().print("403 - forbidden");
                }
            } else {
                response.getWriter().print("404 - page is not found");
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }
}
