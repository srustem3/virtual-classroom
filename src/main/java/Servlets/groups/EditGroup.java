package Servlets.groups;

import Entities.Group;
import Entities.User;
import Repository.AndSpec;
import Repository.group.GroupSpecById;
import Repository.group.GroupSpecByName;
import Repository.group.GroupSpecByTeacher;
import Repository.group.GroupsJpqlRepository;
import checks.CheckAndProcessRussianParameter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "EditGroup", urlPatterns = "/group/edit/*")
public class EditGroup extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Group group = (Group) request.getAttribute("group");
        String name = request.getParameter("name");
        if (name != null && !name.equals("")) {
            User user = (User) request.getAttribute("user");
            List<Group> groupList = GroupsJpqlRepository
                    .getRepository()
                    .query(new AndSpec<>(new GroupSpecByTeacher(user), new GroupSpecByName(name)));
            if (groupList.size() == 0) {
                group.setName(name);
                GroupsJpqlRepository.getRepository().update(group);
                response.sendRedirect("/groups/" + group.getId());
            } else {
                response.sendRedirect(String.format("/groups/%o?message=You already have a group with this name!", group.getId()));
            }
        } else {
            response.sendRedirect(request.getRequestURI() + "?message=Group name should not be empty!");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckAndProcessRussianParameter.check(request);
        request.getRequestDispatcher("/edit_group.ftl").forward(request, response);
    }
}
