package Servlets.groups;

import Entities.Group;
import Entities.User;
import Repository.AndSpec;
import Repository.ISpecification;
import Repository.group.GroupSpecByName;
import Repository.group.GroupSpecByTeacher;
import Repository.group.GroupsJpqlRepository;
import checks.CheckAndProcessRussianParameter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Groups", urlPatterns = "/groups")
public class Groups extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckAndProcessRussianParameter.check(request);
        User user = (User) request.getAttribute("user");
        ISpecification<Group> spec;
        String name = request.getParameter("group_name");
        if (name != null && !name.equals("")) {
            spec = new AndSpec<>(new GroupSpecByName(name), new GroupSpecByTeacher(user));
            request.setAttribute("name", name);
        } else {
            spec = new GroupSpecByTeacher(user);
        }
        request.setAttribute("groups", GroupsJpqlRepository.getRepository().query(spec));
        request.getRequestDispatcher("/templates/groups.ftl").forward(request, response);
    }
}
