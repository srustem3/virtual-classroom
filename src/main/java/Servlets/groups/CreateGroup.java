package Servlets.groups;

import Entities.Group;
import Entities.User;
import Repository.AndSpec;
import Repository.group.GroupSpecByName;
import Repository.group.GroupSpecByTeacher;
import Repository.group.GroupsJpqlRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CreateGroup", urlPatterns = "/create_group")
public class CreateGroup extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        if (name != null && !name.equals("")) {
            User user = (User) request.getAttribute("user");
            List<Group> groupList = GroupsJpqlRepository
                    .getRepository()
                    .query(new AndSpec<>(new GroupSpecByTeacher(user), new GroupSpecByName(name)));
            if (groupList.size() == 0) {
                Group group = new Group();
                group.setName(name);
                group.setTeacher((User) request.getAttribute("user"));
                GroupsJpqlRepository.getRepository().add(group);
                response.sendRedirect("/groups?message=Group is added");
            } else {
                response.sendRedirect("/groups?message=You already have a group with this name!");
            }
        } else {
            response.sendRedirect("/groups?message=Group name should not be empty!");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("message", request.getParameter("message"));
        request.getRequestDispatcher("/create_group.ftl").forward(request, response);
    }
}
