package Servlets.groups;

import Entities.Group;
import Entities.User;
import Repository.group.GroupSpecById;
import Repository.group.GroupsJpqlRepository;
import checks.CheckAndProcessRussianParameter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebFilter(filterName = "EditGroupFilter")
public class EditGroupFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        Pattern pattern = Pattern.compile("/(\\d+)");
        Matcher matcher = pattern.matcher(request.getPathInfo());
        if (matcher.find()) {
            long groupId = Long.parseLong(matcher.group(1));
            List<Group> groupList = GroupsJpqlRepository.getRepository().query(new GroupSpecById(groupId));
            if (groupList.size() == 1) {
                Group group = groupList.get(0);
                User teacher = (User) request.getAttribute("user");
                if (group.getTeacher().getId() == teacher.getId()) {
                    request.setAttribute("group", group);
                    chain.doFilter(req, resp);
                } else {
                    response.getWriter().print("403 - forbidden");
                }
            } else {
                response.getWriter().print("404 - page is not found");
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
