package Servlets.authentication;

import DataBase.DBService;
import Entities.User;
import Repository.OrSpec;
import Repository.user.UserSpecByEmail;
import Repository.user.UserSpecByLogin;
import Repository.user.UsersJpqlRepository;
import checks.CheckAndProcessRussianParameter;
import checks.WrongParameterException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static checks.CheckParameterByRegex.getAndCheckParameter;

@WebServlet(name = "Registration", urlPatterns = "/register")
public class Registration extends javax.servlet.http.HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("UTF-8");
            String email = getAndCheckParameter(request, "email", ".+@.+");
            String login = getAndCheckParameter(request, "login", "[\\d\\w]+");
            String password = getAndCheckParameter(request, "password", ".{6,}");
            String hashedPassword = DatatypeConverter.printHexBinary(
                    MessageDigest.getInstance("SHA-256").digest(password.getBytes("UTF-8")));
            String name = getAndCheckParameter(request, "name", "[\\w]*");
            String surname = getAndCheckParameter(request, "surname", "[\\w]*");
            if (UsersJpqlRepository.getRepository().query(new OrSpec<>(new UserSpecByLogin(login), new UserSpecByEmail(email))).size() == 0) {
                User newUser = new User(email, login, hashedPassword, name, surname);
                UsersJpqlRepository.getRepository().add(newUser);
                response.sendRedirect("/login?message=We created your account!");
            } else {
                response.sendRedirect("/register?message=This user already exists");
            }
        } catch (WrongParameterException | NoSuchAlgorithmException ex) {
            response.sendRedirect("/register?message=" + URLEncoder.encode(ex.getMessage(), "WINDOWS-1251"));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckAndProcessRussianParameter.check(request);
        request.getRequestDispatcher("/templates/register.ftl").forward(request, response);
    }

    @Override
    public void init() throws ServletException {
        super.init();
        DBService.getSessionFactory();
    }

    @Override
    public void destroy() {
        super.destroy();
        DBService.getSessionFactory().close();
    }
}
