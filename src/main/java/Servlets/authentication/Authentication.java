package Servlets.authentication;

import Entities.PersistedSession;
import Entities.User;
import Repository.AndSpec;
import Repository.session.SessionsJpqlRepository;
import Repository.user.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebServlet(name = "Authentication", urlPatterns = {"/login"})
public class Authentication extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        try {
            String hashedPassword = DatatypeConverter.printHexBinary(
                    MessageDigest.getInstance("SHA-256").digest(password.getBytes("UTF-8")));
            List<User> user = UsersJpqlRepository.getRepository().query(new AndSpec<>(new UserSpecByLogin(login),
                    new UserSpecByPassword(hashedPassword)));
            if (user.size() == 1) {
                HttpSession session = request.getSession();
                long userId = user.get(0).getId();
                session.setAttribute("userId", userId);
                if (request.getParameter("rememberMe") != null) {
                    // Создаём и добавляем пользователю новый куки
                    Cookie authenticationCookie = new Cookie("authentication_id", session.getId());
                    authenticationCookie.setMaxAge(24 * 60 * 60);
                    response.addCookie(authenticationCookie);
                    // Сохраняем куки в базу
                    User userToRemember = UsersJpqlRepository.getRepository().query(new UserSpecById(userId)).get(0);
                    PersistedSession persistedSession = new PersistedSession(userToRemember, session.getId());
                    SessionsJpqlRepository.getRepository().add(persistedSession);
                }
                response.sendRedirect("/my_tasks");
            } else {
                response.sendRedirect("/login?message=Wrong login or password");
            }
        } catch (NoSuchAlgorithmException e) {
            response.sendRedirect("/login?message=Something gone wrong...");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = request.getParameter("message");
        request.setAttribute("message", message);
        request.getRequestDispatcher("/templates/login.ftl").forward(request, response);
    }
}
