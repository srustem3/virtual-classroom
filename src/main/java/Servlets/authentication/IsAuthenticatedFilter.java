package Servlets.authentication;

import Entities.PersistedSession;
import Entities.User;
import Repository.session.SessionSpecByJsessionId;
import Repository.session.SessionsJpqlRepository;
import Repository.user.UserSpecById;
import Repository.user.UsersJpqlRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebFilter(filterName = "IsAuthenticatedFilter", servletNames = {"Profile", "CreateGroup", "Groups", "Tasks",
        "CreateApplication", "Applicants", "GroupProfile", "CreateTask", "TeacherTasks", "Answers",
        "TaskProfile", "ViewAnswer"})
public class IsAuthenticatedFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();
        Long userId = (Long) session.getAttribute("userId");

        Long cookieUserId = null;
        Cookie[] cookies = request.getCookies();
        Cookie authenticationCookie = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("authentication_id")) {
                    authenticationCookie = cookie;
                }
            }
        }
        if (authenticationCookie != null) {
            List<PersistedSession> list = SessionsJpqlRepository.getRepository().query(
                    new SessionSpecByJsessionId(authenticationCookie.getValue()));
            if (list.size() == 1) {
                cookieUserId = list.get(0).getUser().getId();
            }
        }
        if (userId != null || cookieUserId != null) {
            Long userIdToUse = userId != null ? userId: cookieUserId;
            List<User> userList = UsersJpqlRepository.getRepository().query(new UserSpecById(userIdToUse));
            if (userList.size() == 1) {
                req.setAttribute("user", userList.get(0));
                chain.doFilter(req, resp);
            } else {
                response.sendRedirect("/login?message=Your account has been removed");
            }
        } else {
            response.sendRedirect("/login?message=You are not authenticated");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
