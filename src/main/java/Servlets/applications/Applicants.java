package Servlets.applications;

import Entities.Application;
import Entities.Group;
import Entities.StudentGroup;
import Entities.User;
import Repository.AndSpec;
import Repository.ISpecification;
import Repository.application.ApplicationSpecByApplicant;
import Repository.application.ApplicationSpecByApplicantLogin;
import Repository.application.ApplicationSpecByTeacher;
import Repository.application.ApplicationsJpqlRepository;
import Repository.group.GroupSpecById;
import Repository.group.GroupSpecByTeacher;
import Repository.group.GroupsJpqlRepository;
import Repository.studentGroup.StudentGroupJpqlRepository;
import Repository.user.UserSpecById;
import Repository.user.UsersJpqlRepository;
import checks.CheckAndProcessRussianParameter;
import org.hibernate.exception.ConstraintViolationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "Applicants", urlPatterns = "/applicants")
public class Applicants extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String option = request.getParameter("option");
        String sStudentId = request.getParameter("student");
        Pattern digitPattern = Pattern.compile("\\d+");
        if (option != null && digitPattern.matcher(sStudentId).matches()) {
            int studentId = Integer.parseInt(sStudentId);
            List<User> studentList = UsersJpqlRepository.getRepository().query(new UserSpecById(studentId));
            if (studentList.size() == 1) {
                if (option.equals("accept")) {
                    String sGroupId = request.getParameter("group");
                    if (sGroupId != null && digitPattern.matcher(sGroupId).matches()) {
                        int groupId = Integer.parseInt(sGroupId);
                        List<Group> groupList = GroupsJpqlRepository.getRepository().query(new GroupSpecById(groupId));
                        if (groupList.size() == 1) {
                            StudentGroup newStudentGroup = new StudentGroup(studentList.get(0), groupList.get(0));
                            try {
                                StudentGroupJpqlRepository.getRepository().add(newStudentGroup);
                                deleteApplication(studentList.get(0), (User) request.getAttribute("user"));
                                response.sendRedirect("/applicants?message=Student is added");
                            } catch (ConstraintViolationException ex) { // Duplicate entry
                                response.sendRedirect("/applicants?message=You already have this student in this group");
                            }
                        } else {
                            response.sendRedirect("/applicants?message=Group is not found");
                        }
                    } else {
                        response.sendRedirect("/applicants?message=Group is not found");
                    }
                } else {
                    // The application is rejected, just delete it
                    deleteApplication(studentList.get(0), (User) request.getAttribute("user"));
                    response.sendRedirect("/applicants?message=The applications is successfully rejected");
                }
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User teacher = (User) request.getAttribute("user");
        String loginFilter = request.getParameter("login_filter");
        ISpecification<Application> spec;
        if (loginFilter != null && !loginFilter.equals("")) {
            request.setAttribute("login", loginFilter);
            spec = new AndSpec<>(new ApplicationSpecByApplicantLogin(loginFilter),
                    new ApplicationSpecByTeacher(teacher));
        } else {
            spec = new ApplicationSpecByTeacher(teacher);
        }
        List<Application> applications = ApplicationsJpqlRepository
                .getRepository()
                .query(spec);
        request.setAttribute("applications", applications);
        request.setAttribute("groups", GroupsJpqlRepository.getRepository().query(new GroupSpecByTeacher(teacher)));
        CheckAndProcessRussianParameter.check(request);
        request.getRequestDispatcher("/templates/applicants.ftl").forward(request, response);
    }

    private void deleteApplication(User student, User teacher) {
        List<Application> applicationList = ApplicationsJpqlRepository.getRepository().query(new AndSpec<>(
                new ApplicationSpecByTeacher(teacher), new ApplicationSpecByApplicant(student)));
        if (applicationList.size() == 1) {
            ApplicationsJpqlRepository.getRepository().remove(applicationList.get(0));
        }
    }
}
