package Servlets.applications;

import Entities.Application;
import Entities.User;
import Repository.ISpecification;
import Repository.TrueSpec;
import Repository.application.ApplicationsJpqlRepository;
import Repository.user.UserSpecByLogin;
import Repository.user.UsersJpqlRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CreateApplication", urlPatterns = "/create_application")
public class CreateApplication extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String teacherLogin = request.getParameter("name");
        String text = request.getParameter("text");
        if (teacherLogin != null && !teacherLogin.equals("") && text != null) {
            User user = (User) request.getAttribute("user");
            List<User> teacherList = UsersJpqlRepository.getRepository().query(new UserSpecByLogin(teacherLogin));
            if (teacherList.size() == 1 && teacherList.get(0).getId() != user.getId()) {
                Application newApplication = new Application(text, teacherList.get(0), user);
                ApplicationsJpqlRepository.getRepository().add(newApplication);
                response.sendRedirect("/create_application?message=Application is sent");
            } else {
                response.sendRedirect("/create_application?message=This teacher does not exist!");
            }
        } else {
            response.sendRedirect("/create_application?message=Teacher's login should not be empty!");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("message", request.getParameter("message"));
        String teacherFilter = request.getParameter("teacher_filter");
        ISpecification<User> spec;
        if (teacherFilter != null && !teacherFilter.equals("")) {
            request.setAttribute("login", teacherFilter);
            spec = new UserSpecByLogin(teacherFilter);
        } else {
            spec = new TrueSpec<>();
        }
        request.setAttribute("teachers", UsersJpqlRepository.getRepository().query(spec));
        request.getRequestDispatcher("/templates/create_application.ftl").forward(request, response);
    }
}
