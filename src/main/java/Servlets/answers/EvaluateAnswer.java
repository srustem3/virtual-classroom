package Servlets.answers;

import Entities.Answer;
import Entities.User;
import Repository.answer.AnswerSpecById;
import Repository.answer.AnswersJpqlRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "EvaluateAnswer", urlPatterns = "/answers_evaluate/*")
public class EvaluateAnswer extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sMark = request.getParameter("mark");
        if (Pattern.compile("\\d+(.\\d{1,2})?").matcher(sMark).matches()) {
            float mark = Float.parseFloat(sMark);
            Answer answer = (Answer) request.getAttribute("answer");
            answer.setScore(mark);
            answer.setComment(request.getParameter("comment"));
            AnswersJpqlRepository.getRepository().update(answer);
            response.sendRedirect(String.format("/tasks/%o?message=Mark is added!", answer.getTask().getId()));
        } else {
            response.sendRedirect(request.getRequestURI() + "?massage=Wrong input data!");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/templates/evaluate_answer.ftl").forward(request, response);
    }
}
