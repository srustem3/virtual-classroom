package Servlets.answers;

import Entities.Answer;
import Entities.User;
import Repository.answer.AnswerSpecById;
import Repository.answer.AnswersJpqlRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "ViewAnswer", urlPatterns = "/my_answers/*")
public class ViewAnswer extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Pattern pattern = Pattern.compile("/(\\d+)");
        Matcher matcher = pattern.matcher(request.getPathInfo());
        if (matcher.find()) {
            long answerId = Long.parseLong(matcher.group(1));
            List<Answer> answerList = AnswersJpqlRepository.getRepository().query(new AnswerSpecById(answerId));
            if (answerList.size() == 1) {
                Answer answer = answerList.get(0);
                User user = (User) request.getAttribute("user");
                if (answer.getUser().getStudent().getId() == user.getId()) {
                    request.setAttribute("answer", answer);
                    request.getRequestDispatcher("/templates/view_answer.ftl").forward(request, response);
                } else {
                    response.getWriter().print("403 - forbidden");
                }
            } else {
                response.getWriter().print("404 - page is not found");
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }
}
