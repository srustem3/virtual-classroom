package Servlets.answers;

import Entities.Answer;
import Entities.User;
import Repository.answer.AnswerSpecById;
import Repository.answer.AnswersJpqlRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebFilter(filterName = "EvaluateAnswerFilter")
public class EvaluateAnswerFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        Pattern pattern = Pattern.compile("/(\\d+)");
        Matcher matcher = pattern.matcher(request.getPathInfo());
        if (matcher.find()) {
            long answerId = Long.parseLong(matcher.group(1));
            List<Answer> answerList = AnswersJpqlRepository.getRepository().query(new AnswerSpecById(answerId));
            if (answerList.size() == 1) {
                Answer answer = answerList.get(0);
                User teacher = (User) request.getAttribute("user");
                if (answer.getTask().getGroup().getTeacher().getId() == teacher.getId()) {
                    request.setAttribute("answer", answer);
                    chain.doFilter(req, resp);
                } else {
                    response.getWriter().print("403 - forbidden");
                }
            } else {
                response.getWriter().print("404 - page is not found");
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
