package Servlets.answers;

import Entities.Answer;
import Entities.User;
import Repository.AndSpec;
import Repository.ISpecification;
import Repository.TrueSpec;
import Repository.answer.AnswerSpecByTaskName;
import Repository.answer.AnswerSpecByUser;
import Repository.answer.AnswersJpqlRepository;
import Repository.group.GroupSourceByUser;
import Repository.group.GroupsJpqlRepository;
import checks.CheckFilters;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "Answers", urlPatterns = "/my_answers")
public class Answers extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String taskName = request.getParameter("name_filter");
        String groupId = request.getParameter("group_filter");
        User user = (User) request.getAttribute("user");
        ISpecification<Answer> spec = CheckFilters.checkGroupIdAndNameForAnswer(groupId, taskName, user, request);
        List<Answer> answerList = AnswersJpqlRepository
                .getRepository()
                .query(spec);
        Collections.reverse(answerList);
        request.setAttribute("answers", answerList);
        request.setAttribute("groups", GroupsJpqlRepository
                .getRepository()
                .query(new GroupSourceByUser(user), new TrueSpec<>()));
        request.getRequestDispatcher("/templates/answers.ftl").forward(request, response);
    }
}
