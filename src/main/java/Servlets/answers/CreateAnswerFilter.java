package Servlets.answers;

import Entities.Answer;
import Entities.Task;
import Entities.User;
import Repository.AndSpec;
import Repository.answer.AnswerSpecByTask;
import Repository.answer.AnswerSpecByUser;
import Repository.answer.AnswersJpqlRepository;
import Repository.task.TaskSpecById;
import Repository.task.TasksJpqlRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebFilter(filterName = "CreateAnswerFilter")
public class CreateAnswerFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        Pattern pattern = Pattern.compile("/(\\d+)");
        Matcher matcher = pattern.matcher(request.getPathInfo());
        if (matcher.find()) {
            long taskId = Long.parseLong(matcher.group(1));
            List<Task> taskList = TasksJpqlRepository.getRepository().query(new TaskSpecById(taskId));
            if (taskList.size() == 1) {
                if (taskList.get(0).getDeadline().getTime() > new Date().getTime()) {
                    request.setAttribute("task", taskList.get(0));
                    User student = (User) request.getAttribute("user");
                    request.setAttribute("answerList", AnswersJpqlRepository.getRepository().query(
                            new AndSpec<>(new AnswerSpecByUser(student), new AnswerSpecByTask(taskList.get(0)))));
                    request.setAttribute("task", taskList.get(0));
                    chain.doFilter(req, resp);
                } else {
                    response.sendRedirect("/my_tasks?message=Deadline!");
                }
            } else {
                response.getWriter().print("404 - page is not found");
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
