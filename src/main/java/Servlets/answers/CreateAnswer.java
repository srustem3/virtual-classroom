package Servlets.answers;

import Entities.*;
import Repository.AndSpec;
import Repository.answer.AnswerSpecByTask;
import Repository.answer.AnswerSpecByUser;
import Repository.answer.AnswersJpqlRepository;
import Repository.studentGroup.StudentGroupJpqlRepository;
import Repository.studentGroup.StudentGroupSpecByGroup;
import Repository.studentGroup.StudentGroupSpecByStudent;
import Repository.task.TaskSpecById;
import Repository.task.TasksJpqlRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "CreateAnswer", urlPatterns = "/tasks/do/*")
public class CreateAnswer extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        @SuppressWarnings("unchecked")
        List<Answer> answerList = (List<Answer>) request.getAttribute("answerList");
        if (answerList.size() == 1) {
            Answer answer = answerList.get(0);
            if (answer.getScore() == null) {
                answer.setText(request.getParameter("text"));
                AnswersJpqlRepository.getRepository().update(answer);
                response.sendRedirect("/my_tasks?message=Your answer is edited!");
            } else {
                response.sendRedirect("/my_answers/" + answer.getId());
            }
        } else {
            Task task = (Task) request.getAttribute("task");
            String answerText = request.getParameter("text");
            if (answerText != null && !answerText.equals("")) {
                User student = (User) request.getAttribute("user");
                Group group = task.getGroup();
                List<StudentGroup> studentGroupList = StudentGroupJpqlRepository
                        .getRepository()
                        .query(new AndSpec<>(new StudentGroupSpecByGroup(group), new StudentGroupSpecByStudent(student)));
                if (studentGroupList.size() == 1) {
                    Answer newAnswer = new Answer(request.getParameter("text"), task, studentGroupList.get(0));
                    AnswersJpqlRepository.getRepository().add(newAnswer);
                    response.sendRedirect("/my_tasks?message=Your answer is added!");
                } else {
                    response.sendRedirect("/my_tasks?message=You don't have access to this task because you are not a student of this group");
                }
            } else {
                response.sendRedirect("/my_tasks?message=Answer text should not be empty!");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        @SuppressWarnings("unchecked")
        List<Answer> answerList = (List<Answer>) request.getAttribute("answerList");
        if (answerList.size() == 1) {
            if (answerList.get(0).getScore() == null) {
                request.setAttribute("answer", answerList.get(0));
                request.getRequestDispatcher("/templates/create_answer.ftl").forward(request, response);
            } else {
                response.sendRedirect("/my_answers/" + answerList.get(0).getId());
            }
        } else {
            request.getRequestDispatcher("/templates/create_answer.ftl").forward(request, response);
        }
    }
}
