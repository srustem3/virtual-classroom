package Servlets.tasks;

import Entities.Task;
import Entities.User;
import Repository.AndSpec;
import Repository.ISpecification;
import Repository.TrueSpec;
import Repository.group.GroupSpecByTeacher;
import Repository.group.GroupsJpqlRepository;
import Repository.task.TaskSourceFromTeacher;
import Repository.task.TaskSpecByGroupId;
import Repository.task.TaskSpecByName;
import Repository.task.TasksJpqlRepository;
import checks.CheckAndProcessRussianParameter;
import checks.CheckFilters;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "TeacherTasks", urlPatterns = "/teacher_tasks")
public class TeacherTasks extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("nameFilter");
        String groupId = request.getParameter("groupFilter");
        ISpecification<Task> spec = CheckFilters.checkGroupIdAndName(groupId, name, request);
        User teacher = (User) request.getAttribute("user");
        List<Task> taskList = TasksJpqlRepository
                .getRepository()
                .query(new TaskSourceFromTeacher(teacher), spec);
        Collections.reverse(taskList);
        request.setAttribute("tasks", taskList);
        request.setAttribute("groups", GroupsJpqlRepository
                .getRepository()
                .query(new GroupSpecByTeacher(teacher)));
        CheckAndProcessRussianParameter.check(request);
        request.getRequestDispatcher("/templates/teacher_tasks.ftl").forward(request, response);
    }
}
