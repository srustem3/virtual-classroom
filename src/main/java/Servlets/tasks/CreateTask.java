package Servlets.tasks;

import Entities.Group;
import Entities.Task;
import Entities.User;
import Repository.group.GroupSpecById;
import Repository.group.GroupSpecByTeacher;
import Repository.group.GroupsJpqlRepository;
import Repository.task.TasksJpqlRepository;
import checks.WrongParameterException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static checks.CheckParameterByRegex.getAndCheckParameter;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "CreateTask", urlPatterns = "/create_task")
public class CreateTask extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("UTF-8");
            String name = getAndCheckParameter(request, "name", ".+");
            String sDeadline = getAndCheckParameter(request, "deadline", "\\d{4}-\\d{2}-\\d{2}");
            Date deadline = new SimpleDateFormat("yyyy-MM-dd").parse(sDeadline);
            String description = getAndCheckParameter(request, "description", ".+");
            String groupId = getAndCheckParameter(request, "group", "\\d+");
            List<Group> groupList = GroupsJpqlRepository.getRepository().query(new GroupSpecById(Integer.parseInt(groupId)));
            if (groupList.size() == 1) {
                Task newTask = new Task(name, description, new Date(), deadline, groupList.get(0));
                TasksJpqlRepository.getRepository().add(newTask);
                response.sendRedirect("/teacher_tasks?message=Task is added");
            } else {
                response.sendRedirect("/teacher_tasks?message=Group is not found");
            }
        } catch (WrongParameterException | ParseException ex) {
            response.sendRedirect("/teacher_tasks?message=" + URLEncoder.encode(ex.getMessage(), "WINDOWS-1251"));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User teacher = (User)request.getAttribute("user");
        request.setAttribute("groups", GroupsJpqlRepository.getRepository().query(new GroupSpecByTeacher(teacher)));
        request.getRequestDispatcher("/templates/create_task.ftl").forward(request, response);
    }
}
