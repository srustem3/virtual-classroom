package Servlets.tasks;

import Entities.Answer;
import Entities.Task;
import Entities.User;
import Repository.answer.AnswerSpecByTask;
import Repository.answer.AnswersJpqlRepository;
import Repository.task.TaskSpecById;
import Repository.task.TasksJpqlRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebServlet(name = "TaskProfile", urlPatterns = "/tasks/*")
public class TaskProfile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Pattern pattern = Pattern.compile("/(\\d+)");
        Matcher matcher = pattern.matcher(request.getPathInfo());
        if (matcher.find()) {
            long taskId = Long.parseLong(matcher.group(1));
            List<Task> taskList = TasksJpqlRepository.getRepository().query(new TaskSpecById(taskId));
            if (taskList.size() == 1) {
                User teacher = (User) request.getAttribute("user");
                if (taskList.get(0).getGroup().getTeacher().getId() == teacher.getId()) {
                    if (request.getParameter("delete") == null) {
                        request.setAttribute("task", taskList.get(0));
                        List<Answer> answerList = AnswersJpqlRepository.getRepository().query(new AnswerSpecByTask(taskList.get(0)));
                        request.setAttribute("answers", answerList);
                        request.getRequestDispatcher("/templates/task_profile.ftl").forward(request, response);
                    } else {
                        TasksJpqlRepository.getRepository().remove(taskList.get(0));
                        response.sendRedirect("/teacher_tasks");
                    }
                } else {
                    response.getWriter().print("403 - forbidden");
                }
            } else {
                response.getWriter().print("404 - page is not found");
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }
}
