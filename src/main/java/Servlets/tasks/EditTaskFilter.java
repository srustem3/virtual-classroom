package Servlets.tasks;

import Entities.Task;
import Entities.User;
import Repository.task.TaskSpecById;
import Repository.task.TasksJpqlRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@WebFilter(filterName = "EditTaskFilter")
public class EditTaskFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        Pattern pattern = Pattern.compile("/(\\d+)");
        Matcher matcher = pattern.matcher(request.getPathInfo());
        if (matcher.find()) {
            long taskId = Long.parseLong(matcher.group(1));
            List<Task> taskList = TasksJpqlRepository.getRepository().query(new TaskSpecById(taskId));
            if (taskList.size() == 1) {
                User teacher = (User) request.getAttribute("user");
                Task task = taskList.get(0);
                if (task.getGroup().getTeacher().getId() == teacher.getId()) {
                    request.setAttribute("task", taskList.get(0));
                    chain.doFilter(req, resp);
                } else {
                    response.getWriter().print("403 - forbidden");
                }
            } else {
                response.getWriter().print("404 - page is not found");
            }
        } else {
            response.getWriter().print("404 - page is not found");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
