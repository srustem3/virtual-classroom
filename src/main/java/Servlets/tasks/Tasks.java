package Servlets.tasks;

import Entities.Task;
import Entities.User;
import Repository.AndSpec;
import Repository.ISpecification;
import Repository.TrueSpec;
import Repository.group.GroupSourceByUser;
import Repository.group.GroupSpecByTeacher;
import Repository.group.GroupsJpqlRepository;
import Repository.task.*;
import checks.CheckAndProcessRussianParameter;
import checks.CheckFilters;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@WebServlet(name = "Tasks", urlPatterns = "/my_tasks")
public class Tasks extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CheckAndProcessRussianParameter.check(request);
        String groupId = request.getParameter("group_filter");
        String name = request.getParameter("name_filter");
        ISpecification<Task> spec = CheckFilters.checkGroupIdAndName(groupId, name, request);
        List<Task> taskList = TasksJpqlRepository
                .getRepository()
                .query(new TaskSourceFromUser((User)request.getAttribute("user")),
                        new AndSpec<>(spec, new TaskSpecByNotHavingAnswer()));
        Collections.reverse(taskList);
        request.setAttribute("taskList", taskList);
        request.setAttribute("groups", GroupsJpqlRepository
                .getRepository()
                .query(new GroupSourceByUser((User) request.getAttribute("user")), new TrueSpec<>()));
        request.getRequestDispatcher("/templates/my_tasks.ftl").forward(request, response);
    }
}
