package checks;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class WrongParameterException extends Exception {
    public WrongParameterException(String message) {
        super(message);
    }
}
