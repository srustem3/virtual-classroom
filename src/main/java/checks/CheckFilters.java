package checks;

import Entities.Answer;
import Entities.Task;
import Entities.User;
import Repository.AndSpec;
import Repository.ISpecification;
import Repository.TrueSpec;
import Repository.answer.AnswerSpecByGroupId;
import Repository.answer.AnswerSpecByTaskName;
import Repository.answer.AnswerSpecByUser;
import Repository.task.TaskSpecByGroupId;
import Repository.task.TaskSpecByName;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class CheckFilters {
    public static ISpecification<Task> checkGroupIdAndName(String groupId, String name, HttpServletRequest req) {
        if ((groupId == null || groupId.equals("")) && (name == null || name.equals(""))) {
            req.setAttribute("groupPar", "");
            return new TrueSpec<>();
        } else if (groupId != null && !groupId.equals("") && name != null && !name.equals("")) { // Both parameters are filled
            req.setAttribute("name", name);
            req.setAttribute("groupPar", groupId);
            return new AndSpec<>(new TaskSpecByGroupId(Long.parseLong(groupId)), new TaskSpecByName(name));
        } else if (groupId == null || groupId.equals("")) { // Name parameter is filled
            req.setAttribute("groupPar", "");
            req.setAttribute("name", name);
            return new TaskSpecByName(name);
        }
        req.setAttribute("groupPar", groupId);
        return new TaskSpecByGroupId(Long.parseLong(groupId)); // Group parameter is filled
    }

    public static ISpecification<Answer> checkGroupIdAndNameForAnswer(String groupId, String name, User user, HttpServletRequest req) {
        if ((groupId == null || groupId.equals("")) && (name == null || name.equals(""))) {
            req.setAttribute("groupPar", "");
            return new AnswerSpecByUser(user);
        } else if (groupId != null && !groupId.equals("") && name != null && !name.equals("")) { // Both parameters are filled
            req.setAttribute("name", name);
            req.setAttribute("groupPar", groupId);
            return new AndSpec<>(new AnswerSpecByUser(user),
                    new AndSpec<>(new AnswerSpecByGroupId(Long.parseLong(groupId)), new AnswerSpecByTaskName(name)));
        } else if (groupId == null || groupId.equals("")) { // Name parameter is filled
            req.setAttribute("groupPar", "");
            req.setAttribute("name", name);
            return new AndSpec<>(new AnswerSpecByUser(user), new AnswerSpecByTaskName(name));
        }
        // Group parameter is filled
        req.setAttribute("groupPar", groupId);
        return new AndSpec<>(new AnswerSpecByUser(user), new AnswerSpecByGroupId(Long.parseLong(groupId)));
    }
}
