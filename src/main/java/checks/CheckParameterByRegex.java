package checks;

import Servlets.authentication.Registration;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class CheckParameterByRegex {
    public static String getAndCheckParameter(HttpServletRequest request, String parName, String pattern)
            throws WrongParameterException {
        String parameterValue = request.getParameter(parName);
        if (Pattern.compile(pattern, Pattern.UNICODE_CHARACTER_CLASS).matcher(parameterValue).matches()) {
            return parameterValue;
        } else {
            throw new WrongParameterException(String.format("Ошибка при вводе данных: %s", parName));
        }
    }
}
