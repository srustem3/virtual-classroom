package checks;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class CheckAndProcessRussianParameter {
    public static void check(HttpServletRequest request) throws UnsupportedEncodingException {
        String message = request.getParameter("message");
        if (message != null) {
            message = URLDecoder.decode(message, "WINDOWS-1251");
        }
        request.setAttribute("message", message);
    }
}
