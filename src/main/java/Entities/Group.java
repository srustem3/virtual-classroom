package Entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "groups")
public class Group implements HasId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacher_id")
    private User teacher;

    @OneToMany(mappedBy = "group", orphanRemoval=true)
    private Set<StudentGroup> student_group;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "group", orphanRemoval=true)
    private Set<Task> tasks;

    public Group(String name, User teacher) {
        this.name = name;
        this.teacher = teacher;
    }

    public Group() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public Set<StudentGroup> getStudent_group() {
        return student_group;
    }

    public void setStudent_group(Set<StudentGroup> student_group) {
        this.student_group = student_group;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
}
