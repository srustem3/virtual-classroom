package Entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users", indexes = {
        @Index(columnList = "email", name = "user_email_index"),
        @Index(columnList = "login", name = "user_login_index"),
        @Index(columnList = "surname", name = "user_surname_index")})
public class User implements HasId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "login", nullable = false)
    private String login;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher", orphanRemoval=true)
    private Set<Group> supervisedGroups;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval=true)
    private Set<PersistedSession> sessions;

    @OneToMany(mappedBy = "student", orphanRemoval=true)
    private Set<StudentGroup> student_group;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher", orphanRemoval=true)
    private Set<Application> applicants;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "applicant", orphanRemoval=true)
    private Set<Application> applications;

    public User(String email, String login, String password, String name, String surname) {
        this.email = email;
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
    }

    public User() {
    }

    public Set<PersistedSession> getSessions() {
        return sessions;
    }

    public void setSessions(Set<PersistedSession> sessions) {
        this.sessions = sessions;
    }

    public Set<Application> getApplicants() {
        return applicants;
    }

    public void setApplicants(Set<Application> applicants) {
        this.applicants = applicants;
    }

    public Set<Application> getApplications() {
        return applications;
    }

    public void setApplications(Set<Application> applications) {
        this.applications = applications;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Set<Group> getSupervisedGroups() {
        return supervisedGroups;
    }

    public void setSupervisedGroups(Set<Group> supervisedGroups) {
        this.supervisedGroups = supervisedGroups;
    }

    public Set<StudentGroup> getStudent_group() {
        return student_group;
    }

    public void setStudent_group(Set<StudentGroup> student_group) {
        this.student_group = student_group;
    }
}
