package Entities;

import javax.persistence.*;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@Entity
@Table(name = "sessions")
public class PersistedSession implements HasId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "jSessionId", nullable = false)
    private String jSessionId;

    public PersistedSession() {
    }

    public PersistedSession(User user, String jSessionId) {
        this.user = user;
        this.jSessionId = jSessionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getjSessionId() {
        return jSessionId;
    }

    public void setjSessionId(String jSessionId) {
        this.jSessionId = jSessionId;
    }
}
