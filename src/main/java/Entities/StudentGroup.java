package Entities;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@Entity
@Table(name = "students",
        uniqueConstraints=@UniqueConstraint(columnNames={"user_id", "group_id"}))
public class StudentGroup implements HasId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
    private Group group;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval=true)
    private Set<Answer> answers;

    public StudentGroup(User student, Group group) {
        this.student = student;
        this.group = group;
    }

    public StudentGroup() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getStudent() {
        return student;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }
}
