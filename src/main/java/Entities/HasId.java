package Entities;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface HasId {
    long getId();
}
