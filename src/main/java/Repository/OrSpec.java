package Repository;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class OrSpec<T> implements ISpecification<T> {
    private ISpecification<T> one;
    private ISpecification<T> two;

    public OrSpec(ISpecification<T> one, ISpecification<T> two) {
        this.one = one;
        this.two = two;
    }

    @Override
    public String toJpqlClauses() {
        return String.format("%s or %s", one.toJpqlClauses(), two.toJpqlClauses());
    }

    @Override
    public boolean specified(T t) {
        return one.specified(t) || two.specified(t);
    }

    @Override
    public void setParameters(TypedQuery<T> query) {
        one.setParameters(query);
        two.setParameters(query);
    }
}
