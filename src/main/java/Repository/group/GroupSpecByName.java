package Repository.group;

import Entities.Group;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class GroupSpecByName implements ISpecification<Group> {
    private String name;

    public GroupSpecByName(String name) {
        this.name = name;
    }

    @Override
    public boolean specified(Group group) {
        return group.getName().equals(name);
    }

    @Override
    public String toJpqlClauses() {
        return "e.name=:gn_name";
    }

    @Override
    public void setParameters(TypedQuery<Group> query) {
        query.setParameter("gn_name", name);
    }
}
