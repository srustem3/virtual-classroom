package Repository.group;

import Entities.Group;
import Repository.AbstractJpqlRepository;
import Repository.IRepository;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class GroupsJpqlRepository extends AbstractJpqlRepository<Group> {
    private static IRepository<Group> repository;

    public static IRepository<Group> getRepository() {
        if (repository == null) {
            repository = new GroupsJpqlRepository();
        }
        return repository;
    }

    @Override
    protected Class<Group> getDomain() {
        return Group.class;
    }
}
