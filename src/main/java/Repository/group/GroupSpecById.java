package Repository.group;

import Entities.Group;
import Entities.Task;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class GroupSpecById implements ISpecification<Group> {
    private long id;

    public GroupSpecById(long id) {
        this.id = id;
    }

    @Override
    public boolean specified(Group group) {
        return group.getId() == id;
    }

    @Override
    public String toJpqlClauses() {
        return "id=:gi_id";
    }

    @Override
    public void setParameters(TypedQuery<Group> query) {
        query.setParameter("gi_id", id);
    }
}
