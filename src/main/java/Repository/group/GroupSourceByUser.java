package Repository.group;

import Entities.Group;
import Entities.User;
import Repository.ISource;

import javax.persistence.TypedQuery;
import java.util.stream.Stream;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class GroupSourceByUser implements ISource<Group> {
    private User user;

    public GroupSourceByUser(User user) {
        this.user = user;
    }

    @Override
    public Stream<Group> collectionSource() {
        return null;
    }

    @Override
    public String jpqlSource() {
        return "select e from Group e join e.student_group sg join sg.student s on s.id=:gu_user_id";
    }

    @Override
    public void setParameters(TypedQuery<Group> query) {
        query.setParameter("gu_user_id", user.getId());
    }
}
