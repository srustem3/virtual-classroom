package Repository.group;

import Entities.Group;
import Entities.Task;
import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

public class GroupSpecByTeacher implements ISpecification<Group> {
    private User teacher;

    public GroupSpecByTeacher(User teacher) {
        this.teacher = teacher;
    }

    @Override
    public boolean specified(Group group) {
        return group.getTeacher().getId() == teacher.getId();
    }

    @Override
    public String toJpqlClauses() {
        return "e.teacher.id=:gt_teacher_id";
    }

    @Override
    public void setParameters(TypedQuery<Group> query) {
        query.setParameter("gt_teacher_id", teacher.getId());
    }
}
