package Repository;

import javax.persistence.TypedQuery;

public class AndSpec<T> implements ISpecification<T> {
    private ISpecification<T> one;
    private ISpecification<T> two;

    public AndSpec(ISpecification<T> one, ISpecification<T> two) {
        this.one = one;
        this.two = two;
    }

    @Override
    public String toJpqlClauses() {
        return String.format("%s and %s", one.toJpqlClauses(), two.toJpqlClauses());
    }

    @Override
    public boolean specified(T t) {
        return one.specified(t) && two.specified(t);
    }

    @Override
    public void setParameters(TypedQuery<T> query) {
        one.setParameters(query);
        two.setParameters(query);
    }
}
