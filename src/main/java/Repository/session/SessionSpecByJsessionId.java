package Repository.session;

import Entities.PersistedSession;
import Entities.Task;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class SessionSpecByJsessionId implements ISpecification<PersistedSession> {
    private String jSessionId;

    public SessionSpecByJsessionId(String jSessionId) {
        this.jSessionId = jSessionId;
    }

    @Override
    public boolean specified(PersistedSession persistedSession) {
        return persistedSession.getjSessionId().equals(jSessionId);
    }

    @Override
    public String toJpqlClauses() {
        return "jSessionId=:jsess";
    }

    @Override
    public void setParameters(TypedQuery<PersistedSession> query) {
        query.setParameter("jsess", jSessionId);
    }
}
