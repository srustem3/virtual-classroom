package Repository.session;

import Entities.PersistedSession;
import Repository.AbstractJpqlRepository;
import Repository.IRepository;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class SessionsJpqlRepository extends AbstractJpqlRepository<PersistedSession> {
    private static IRepository<PersistedSession> repository;

    public static IRepository<PersistedSession> getRepository() {
        if (repository == null) {
            repository = new SessionsJpqlRepository();
        }
        return repository;
    }

    @Override
    protected Class<PersistedSession> getDomain() {
        return PersistedSession.class;
    }
}
