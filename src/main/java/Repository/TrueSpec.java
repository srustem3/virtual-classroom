package Repository;

import Entities.Group;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class TrueSpec<T> implements ISpecification<T> {
    @Override
    public boolean specified(T o) {
        return true;
    }

    @Override
    public String toJpqlClauses() {
        return "1=1";
    }

    @Override
    public void setParameters(TypedQuery<T> query) {

    }
}
