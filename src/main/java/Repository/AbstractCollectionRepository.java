package Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class AbstractCollectionRepository<T> implements IRepository<T> {
    private List<T> entities;

    public AbstractCollectionRepository(List<T> entities) {
        this.entities = entities;
    }

    @Override
    public void add(T entity) {
        entities.add(entity);
    }

    @Override
    public void remove(T entity) {
        entities.remove(entity);
    }

    @Override
    public void update(T entity) {
    }

    @Override
    public List<T> query(ISource<T> source, ISpecification<T> spec) {
        return source
                .collectionSource()
                .filter(spec::specified)
                .collect(Collectors.toList());
    }

    @Override
    public List<T> query(ISpecification<T> spec) {
        return entities
                .stream()
                .filter(spec::specified)
                .collect(Collectors.toList());
    }
}
