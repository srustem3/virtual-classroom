package Repository;

public interface ISpecification<T> extends HasParameters<T> {
    boolean specified(T t);
    String toJpqlClauses();
}
