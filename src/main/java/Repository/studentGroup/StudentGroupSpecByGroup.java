package Repository.studentGroup;

import Entities.Group;
import Entities.StudentGroup;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class StudentGroupSpecByGroup implements ISpecification<StudentGroup> {
    private Group group;

    public StudentGroupSpecByGroup(Group group) {
        this.group = group;
    }

    @Override
    public boolean specified(StudentGroup studentGroup) {
        return studentGroup.getGroup().getId() == group.getId();
    }

    @Override
    public String toJpqlClauses() {
        return "e.group.id=:sg_group_id";
    }

    @Override
    public void setParameters(TypedQuery<StudentGroup> query) {
        query.setParameter("sg_group_id", group.getId());
    }
}
