package Repository.studentGroup;

import Entities.StudentGroup;
import Repository.AbstractJpqlRepository;
import Repository.IRepository;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class StudentGroupJpqlRepository extends AbstractJpqlRepository<StudentGroup> {
    private static IRepository<StudentGroup> repository;

    public static IRepository<StudentGroup> getRepository() {
        if (repository == null) {
            repository = new StudentGroupJpqlRepository();
        }
        return repository;
    }

    @Override
    protected Class<StudentGroup> getDomain() {
        return StudentGroup.class;
    }
}
