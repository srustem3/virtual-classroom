package Repository.studentGroup;

import Entities.StudentGroup;
import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class StudentGroupSpecByStudent implements ISpecification<StudentGroup> {
    private User student;

    public StudentGroupSpecByStudent(User student) {
        this.student = student;
    }

    @Override
    public boolean specified(StudentGroup studentGroup) {
        return studentGroup.getStudent().getId() == student.getId();
    }

    @Override
    public String toJpqlClauses() {
        return "e.student.id=:sg_student_id";
    }

    @Override
    public void setParameters(TypedQuery<StudentGroup> query) {
        query.setParameter("sg_student_id", student.getId());
    }
}
