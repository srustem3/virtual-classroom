package Repository.user;

import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class UserSpecByEmail implements ISpecification<User> {
    private String email;

    public UserSpecByEmail(String email) {
        this.email = email;
    }

    @Override
    public String toJpqlClauses() {
        return "e.email=:u_email";
    }

    @Override
    public boolean specified(User entity) {
        return entity.getEmail().equals(email);
    }

    @Override
    public void setParameters(TypedQuery<User> query) {
        query.setParameter("u_email", email);
    }
}
