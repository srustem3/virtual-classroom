package Repository.user;

import Entities.User;
import Repository.AbstractJpqlRepository;
import Repository.IRepository;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class UsersJpqlRepository extends AbstractJpqlRepository<User> {
    private static IRepository<User> repository;

    public static IRepository<User> getRepository() {
        if (repository == null) {
            repository = new UsersJpqlRepository();
        }
        return repository;
    }

    @Override
    protected Class<User> getDomain() {
        return User.class;
    }
}
