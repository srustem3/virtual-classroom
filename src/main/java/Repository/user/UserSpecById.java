package Repository.user;

import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

public class UserSpecById implements ISpecification<User> {
    private long id;

    public UserSpecById(long id) {
        this.id = id;
    }

    @Override
    public String toJpqlClauses() {
        return "e.id=:u_id";
    }

    @Override
    public boolean specified(User user) {
        return user.getId() == id;
    }

    @Override
    public void setParameters(TypedQuery<User> query) {
        query.setParameter("u_id", id);
    }
}
