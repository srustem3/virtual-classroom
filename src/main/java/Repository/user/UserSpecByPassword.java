package Repository.user;

import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

public class UserSpecByPassword implements ISpecification<User> {
    private String password;

    public UserSpecByPassword(String password) {
        this.password = password;
    }

    @Override
    public String toJpqlClauses() {
        return "e.password=:u_password";
    }

    @Override
    public boolean specified(User entity) {
        return entity.getPassword().equals(password);
    }

    @Override
    public void setParameters(TypedQuery<User> query) {
        query.setParameter("u_password", password);
    }
}
