package Repository.user;

import Entities.Group;
import Entities.StudentGroup;
import Entities.User;
import Repository.ISource;

import javax.persistence.TypedQuery;
import java.util.stream.Stream;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class UserSourceFromGroup implements ISource<User> {
    private Group group;

    public UserSourceFromGroup(Group group) {
        this.group = group;
    }

    @Override
    public Stream<User> collectionSource() {
        return group
                .getStudent_group()
                .stream()
                .map(StudentGroup::getStudent);
    }

    @Override
    public String jpqlSource() {
        return "select sg.student from Group g join g.student_group sg on g.id=:u_group_id";
    }

    @Override
    public void setParameters(TypedQuery<User> query) {
        query.setParameter("u_group_id", group.getId());
    }
}
