package Repository.user;

import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

public class UserSpecByLogin implements ISpecification<User> {
    private String login;
    private String loginType;

    public UserSpecByLogin(String login) {
        this.login = login;
        loginType = login.contains("@") ? "e.email" : "e.login";
    }

    @Override
    public String toJpqlClauses() {
        return String.format("%s=:u_login", loginType);
    }

    @Override
    public boolean specified(User entity) {
        if (loginType.equals("e.email")) {
            return entity.getEmail().equals(login);
        } else {
            return entity.getLogin().equals(login);
        }
    }

    @Override
    public void setParameters(TypedQuery<User> query) {
        query.setParameter("u_login", login);
    }
}
