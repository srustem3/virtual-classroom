package Repository;

import DataBase.DBService;
import Entities.HasId;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public abstract class AbstractJpqlRepository<T extends HasId> implements IRepository<T> {
    protected abstract Class<T> getDomain();

    @Override
    public void add(T entity) {
        try (Session session = DBService.getSessionFactory().openSession()) {
            session.saveOrUpdate(entity);
        }
    }

    @Override
    public void remove(T entity) {
        try (Session session = DBService.getSessionFactory().openSession()) {
            Transaction tr = session.beginTransaction();
            session.remove(session.find(getDomain(), entity.getId()));
            tr.commit();
        }
    }

    @Override
    public void update(T entity) {
        try (Session session = DBService.getSessionFactory().openSession()) {
            Transaction tr = session.beginTransaction();
            session.saveOrUpdate(entity);
            tr.commit();
        }
    }

    @Override
    public List<T> query(ISource<T> source, ISpecification<T> spec) {
        try (Session session = DBService.getSessionFactory().openSession()) {
            TypedQuery<T> query = session.createQuery(String.format("%s where %s", source.jpqlSource(), spec.toJpqlClauses()),
                    getDomain());
            spec.setParameters(query);
            source.setParameters(query);
            return query.getResultList();
        }
    }

    @Override
    public List<T> query(ISpecification<T> spec) {
        try (Session session = DBService.getSessionFactory().openSession()) {
            TypedQuery<T> query = session.createQuery(String.format("select e from %s e where %s",
                    getDomain().getSimpleName(), spec.toJpqlClauses()), getDomain());
            spec.setParameters(query);
            return query.getResultList();
        }
    }
}
