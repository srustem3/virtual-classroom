package Repository.task;

import Entities.Task;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class TaskSpecById implements ISpecification<Task> {
    private long id;

    public TaskSpecById(long id) {
        this.id = id;
    }

    @Override
    public boolean specified(Task task) {
        return task.getId() == id;
    }

    @Override
    public String toJpqlClauses() {
        return "e.id=:ti_id";
    }

    @Override
    public void setParameters(TypedQuery<Task> query) {
        query.setParameter("ti_id", id);
    }
}
