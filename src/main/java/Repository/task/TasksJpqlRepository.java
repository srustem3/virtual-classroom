package Repository.task;

import Entities.Task;
import Repository.AbstractJpqlRepository;
import Repository.IRepository;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class TasksJpqlRepository extends AbstractJpqlRepository<Task> {
    private static IRepository<Task> repository;

    public static IRepository<Task> getRepository() {
        if (repository == null) {
            repository = new TasksJpqlRepository();
        }
        return repository;
    }

    @Override
    protected Class<Task> getDomain() {
        return Task.class;
    }
}
