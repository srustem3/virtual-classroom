package Repository.task;

import Entities.Group;
import Entities.Task;
import Entities.User;
import Repository.ISource;

import javax.persistence.TypedQuery;
import java.util.stream.Stream;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class TaskSourceFromTeacher implements ISource<Task> {
    private User teacher;

    public TaskSourceFromTeacher(User teacher) {
        this.teacher = teacher;
    }

    @Override
    public Stream<Task> collectionSource() {
        return teacher
                .getSupervisedGroups()
                .stream()
                .flatMap(group -> group.getTasks().stream());
    }

    @Override
    public String jpqlSource() {
        return "select e from User u join u.supervisedGroups g " +
                "on u.id=:tu_teacher_id join g.tasks e";
    }

    @Override
    public void setParameters(TypedQuery<Task> query) {
        query.setParameter("tu_teacher_id", teacher.getId());
    }
}
