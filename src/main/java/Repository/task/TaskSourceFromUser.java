package Repository.task;

import Entities.StudentGroup;
import Entities.Task;
import Entities.User;
import Repository.ISource;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class TaskSourceFromUser implements ISource<Task> {
    private User user;

    public TaskSourceFromUser(User user) {
        this.user = user;
    }

    @Override
    public Stream<Task> collectionSource() {
        return user.getStudent_group()
                .stream()
                .map(StudentGroup::getGroup)
                .flatMap(group -> group.getTasks().stream());
    }

    @Override
    public String jpqlSource() {
        return "select e from User u join u.student_group sg on u.id=:tu_user_id" +
                " join sg.group.tasks e" +
                " left join e.answers a on a.user.student.id=:tu_user_id";
    }

    @Override
    public void setParameters(TypedQuery<Task> query) {
        query.setParameter("tu_user_id", user.getId());
    }
}
