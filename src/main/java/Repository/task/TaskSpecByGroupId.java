package Repository.task;

import Entities.Group;
import Entities.Task;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class TaskSpecByGroupId implements ISpecification<Task> {
    private long groupId;

    public TaskSpecByGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean specified(Task task) {
        return task.getGroup().getId() == groupId;
    }

    @Override
    public String toJpqlClauses() {
        return "e.group.id=:tg_group_id";
    }

    @Override
    public void setParameters(TypedQuery<Task> query) {
        query.setParameter("tg_group_id", groupId);
    }
}
