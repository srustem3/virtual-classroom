package Repository.task;

import Entities.Task;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class TaskSpecByName implements ISpecification<Task> {
    private String name;

    public TaskSpecByName(String name) {
        this.name = name;
    }

    @Override
    public boolean specified(Task task) {
        return task.getName().equals(name);
    }

    @Override
    public String toJpqlClauses() {
        return "e.name=:tn_name";
    }

    @Override
    public void setParameters(TypedQuery<Task> query) {
        query.setParameter("tn_name", name);
    }
}
