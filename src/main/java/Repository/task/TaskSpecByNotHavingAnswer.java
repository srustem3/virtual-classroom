package Repository.task;

import Entities.Task;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * Specification only for TaskSourceFromUser
 */
public class TaskSpecByNotHavingAnswer implements ISpecification<Task> {
    @Override
    public boolean specified(Task task) {
        return false;
    }

    @Override
    public String toJpqlClauses() {
        return "a is null";
    }

    @Override
    public void setParameters(TypedQuery<Task> query) {

    }
}
