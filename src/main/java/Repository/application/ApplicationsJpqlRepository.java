package Repository.application;

import Entities.Application;
import Repository.AbstractJpqlRepository;
import Repository.IRepository;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class ApplicationsJpqlRepository extends AbstractJpqlRepository<Application> {
    private static IRepository<Application> repository;

    public static IRepository<Application> getRepository() {
        if (repository == null) {
            repository = new ApplicationsJpqlRepository();
        }
        return repository;
    }

    @Override
    protected Class<Application> getDomain() {
        return Application.class;
    }
}
