package Repository.application;

import Entities.Application;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class ApplicationSpecByApplicantLogin implements ISpecification<Application> {
    private String login;

    public ApplicationSpecByApplicantLogin(String login) {
        this.login = login;
    }

    @Override
    public boolean specified(Application application) {
        return application.getApplicant().getLogin().equals(login);
    }

    @Override
    public String toJpqlClauses() {
        return "e.applicant.login=:app_applicant_login";
    }

    @Override
    public void setParameters(TypedQuery<Application> query) {
        query.setParameter("app_applicant_login", login);
    }
}
