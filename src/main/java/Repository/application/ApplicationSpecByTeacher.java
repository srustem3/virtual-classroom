package Repository.application;

import Entities.Application;
import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class ApplicationSpecByTeacher implements ISpecification<Application> {
    private User teacher;

    public ApplicationSpecByTeacher(User teacher) {
        this.teacher = teacher;
    }

    @Override
    public boolean specified(Application application) {
        return application.getTeacher().getId() == teacher.getId();
    }

    @Override
    public String toJpqlClauses() {
        return "teacher.id=:app_teacher_id";
    }

    @Override
    public void setParameters(TypedQuery<Application> query) {
        query.setParameter("app_teacher_id", teacher.getId());
    }
}
