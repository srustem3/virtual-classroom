package Repository.application;

import Entities.Application;
import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class ApplicationSpecByApplicant implements ISpecification<Application> {
    private User applicant;

    public ApplicationSpecByApplicant(User applicant) {
        this.applicant = applicant;
    }

    @Override
    public boolean specified(Application application) {
        return application.getApplicant().getId() == applicant.getId();
    }

    @Override
    public String toJpqlClauses() {
        return "applicant.id=:app_applicant_id";
    }

    @Override
    public void setParameters(TypedQuery<Application> query) {
        query.setParameter("app_applicant_id", applicant.getId());
    }
}
