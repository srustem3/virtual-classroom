package Repository;

import java.util.List;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface IRepository<T> {
    void add(T entity);
    void remove(T entity);
    void update(T entity);
    List<T> query(ISource<T> source, ISpecification<T> spec);
    List<T> query(ISpecification<T> spec);
}
