package Repository;

import javax.persistence.TypedQuery;
import java.util.stream.Stream;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface ISource<T> extends HasParameters<T> {
    Stream<T> collectionSource();
    String jpqlSource();
}
