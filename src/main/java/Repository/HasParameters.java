package Repository;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface HasParameters<T> {
    void setParameters(TypedQuery<T> query);
}
