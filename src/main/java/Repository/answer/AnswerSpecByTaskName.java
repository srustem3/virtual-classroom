package Repository.answer;

import Entities.Answer;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class AnswerSpecByTaskName implements ISpecification<Answer> {
    private String taskName;

    public AnswerSpecByTaskName(String taskName) {
        this.taskName = taskName;
    }

    @Override
    public boolean specified(Answer answer) {
        return answer.getTask().getName().equals(taskName);
    }

    @Override
    public String toJpqlClauses() {
        return "e.task.name=:ans_task_name";
    }

    @Override
    public void setParameters(TypedQuery<Answer> query) {
        query.setParameter("ans_task_name", taskName);
    }
}
