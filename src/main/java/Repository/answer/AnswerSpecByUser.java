package Repository.answer;

import Entities.Answer;
import Entities.User;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class AnswerSpecByUser implements ISpecification<Answer> {
    private User user;

    public AnswerSpecByUser(User user) {
        this.user = user;
    }

    @Override
    public boolean specified(Answer answer) {
        return answer.getUser().getStudent().getId() == user.getId();
    }

    @Override
    public String toJpqlClauses() {
        return "e.user.student.id=:ans_user_id";
    }

    @Override
    public void setParameters(TypedQuery<Answer> query) {
        query.setParameter("ans_user_id", user.getId());
    }
}
