package Repository.answer;

import Entities.Answer;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class AnswerSpecByGroupId implements ISpecification<Answer> {
    private long groupId;

    public AnswerSpecByGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean specified(Answer answer) {
        return answer.getTask().getGroup().getId() == groupId;
    }

    @Override
    public String toJpqlClauses() {
        return "e.task.group.id=:ans_group_id";
    }

    @Override
    public void setParameters(TypedQuery<Answer> query) {
        query.setParameter("ans_group_id", groupId);
    }
}
