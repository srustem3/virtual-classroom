package Repository.answer;

import Entities.Answer;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class AnswerSpecById implements ISpecification<Answer> {
    private long id;

    public AnswerSpecById(long id) {
        this.id = id;
    }

    @Override
    public boolean specified(Answer answer) {
        return answer.getId() == id;
    }

    @Override
    public String toJpqlClauses() {
        return "e.id=:ans_id";
    }

    @Override
    public void setParameters(TypedQuery<Answer> query) {
        query.setParameter("ans_id", id);
    }
}
