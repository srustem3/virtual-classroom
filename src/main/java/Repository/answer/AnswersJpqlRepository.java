package Repository.answer;

import Entities.Answer;
import Repository.AbstractJpqlRepository;
import Repository.IRepository;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class AnswersJpqlRepository extends AbstractJpqlRepository<Answer> {
    private static IRepository<Answer> repository;

    public static IRepository<Answer> getRepository() {
        if (repository == null) {
            repository = new AnswersJpqlRepository();
        }
        return repository;
    }

    @Override
    protected Class<Answer> getDomain() {
        return Answer.class;
    }
}
