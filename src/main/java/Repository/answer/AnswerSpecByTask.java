package Repository.answer;

import Entities.Answer;
import Entities.Task;
import Repository.ISpecification;

import javax.persistence.TypedQuery;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class AnswerSpecByTask implements ISpecification<Answer> {
    private Task task;

    public AnswerSpecByTask(Task task) {
        this.task = task;
    }

    @Override
    public boolean specified(Answer answer) {
        return answer.getTask().getId() == task.getId();
    }

    @Override
    public String toJpqlClauses() {
        return "e.task.id=:ans_task_id";
    }

    @Override
    public void setParameters(TypedQuery<Answer> query) {
        query.setParameter("ans_task_id", task.getId());
    }
}
