<#list groups>
<p>Groups:
<ul>
    <#items as group>
        <li><a href="/groups/${group.getId()}">${group.getName()}</a></li>
    </#items>
</ul>
<#else>
    <p>You have no groups where you are a teacher.
</#list>