<html>
<head>
    <title>Создание группы</title>
</head>
<body>
<#if message??><h1>${message}</h1></#if>
<form action="/create_application" method="post">
    <label for="name">Логин преподавателя</label>
    <input type="text" id="name" name="name" required><br>
    <label for="text">Текст заявки</label>
    <textarea id="text" name="text"></textarea>
    <input type="submit" value="Подать заявку">
</form>
</body>
</html>