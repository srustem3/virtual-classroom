<html>
<head>
    <title>Группа ${group.getName()}</title>
</head>
<body>
${group.getId()}<br>
${group.getName()}<br>
<#list studentList>
<p>Students:
<ul>
    <#items as student>
        <li>${student.getLogin()}</li>
    </#items>
</ul>
<#else>
<p>This group has no students.
</#list>
<form><input type="submit" name="delete" value="Delete"></form>
<a href="/group/edit/${group.getId()}">Редактировать</a>
</body>
</html>