<#list answers>
<p>Answers:</p>
<ul>
    <#items as answer>
        <li><a href="/my_answers/${answer.getId()}">${answer.getUser().getLogin()}</a></li>
    </#items>
</ul>
<#else>
<p>You have no answers at all :(</p>
</#list>