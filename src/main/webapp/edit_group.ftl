<html>
<head>
    <title>Редактирование группы</title>
</head>
<body>
<#if message??><h1>${message}</h1></#if>
<form method="post">
    <label for="name">Название группы</label>
    <input type="text" id="name" name="name" value="${group.getName()}" required><br>
    <input type="submit" value="Изменить название">
</form>
</body>
</html>