<#if message??><h1>${message}</h1></#if>
<#list tasks>
<p>My tasks:
<ul>
    <#items as task>
        <li><a href="/tasks/${task.getId()}">${task.getName()}</a></li>
    </#items>
</ul>
<#else>
<p>You have no own tasks.
</#list>