<html>
<head>
    <title>Редактирование задания</title>
</head>
<body>
<#if message??><h1>${message}</h1></#if>
<form method="post">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" value="${task.getName()}" required><br>
    <label for="deadline">Deadline</label>
    <input type="date" id="deadline" name="deadline" value="${task.getDeadline()?datetime?string('yyyy-MM-dd')}" required><br>
    <label for="group">Group</label>
    <select id="group" name="group" required>
    <#list groups>
        <option disabled>Choose a group</option>
        <#items as group>
            <#if task.getGroup().getId() == group.getId()>
                <option value="${group.getId()}" selected="selected">${group.getName()}</option>
            <#else>
                <option value="${group.getId()}">${group.getName()}</option>
        </#if>
        </#items>
    <#else>
        <option disabled>You have no own groups now.</option>
    </#list>
    </select><br>
    <label for="description">Description</label>
    <textarea id="description" name="description" required><#if task.getDescription()??>${task.getDescription()}</#if></textarea><br>
    <input type="submit" value="Edit task">
</form>
</body>
</html>