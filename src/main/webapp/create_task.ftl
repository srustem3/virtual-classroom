<html>
<head>
    <title>Создание задания</title>
</head>
<body>
<#if message??><h1>${message}</h1></#if>
<form action="/create_task" method="post">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" required><br>
    <label for="deadline">Deadline</label>
    <input type="date" id="deadline" name="deadline" required><br>
    <label for="group">Group</label>
    <select id="group" name="group" required>
        <#list groups>
            <option disabled>Choose a group</option>
                <#items as group>
                    <option value="${group.getId()}">${group.getName()}</option>
                </#items>
        <#else>
            <option disabled>You have no own groups now.</option>
        </#list>
    </select><br>
    <label for="description">Description</label>
    <textarea id="description" name="description" required></textarea><br>
    <input type="submit" value="Create task">
</form>
</body>
</html>