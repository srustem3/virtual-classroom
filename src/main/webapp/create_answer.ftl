Task's id: ${task.getId()}<br>
Task's name: ${task.getName()}<br>
Task's description: <#if task.getDescription??>${task.getDescription()}<#else>No</#if><br>
Task's creation date: ${task.getDateTime()}<br>
Task's deadline: <#if task.getDeadline??>${task.getDeadline()}<#else>No</#if><br>
<form method="post">
    <label for="text">Ответ</label>
    <textarea id="text" name="text"><#if answer??>${answer.getText()}</#if></textarea>
    <input type="submit" value="Ответить">
</form>