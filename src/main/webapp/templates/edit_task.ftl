<#include "base_teacher.ftl">
<#macro is_tasks> class="active"</#macro>
<#macro style>
@media screen and (max-width: 589px){
.btn_black{
font-size: 12px;
}
}
@media screen and (max-width: 410px){
.btn_black{
font-size: 8px;
}
}
</#macro>
<#macro css>
<link rel="stylesheet" type="text/css" href="/css/css_reg.css">
<link rel="stylesheet" type="text/css" href="/css/css_group_create.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro content>
<div class="container container_body">
    <div class="jumbotron">
        <#if message??><h2>${message}</h2></#if>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap">
                    <form class="form-horizontal login" method="post">
                        <fieldset class="fieldset">
                            <p class="form-title color" style="margin-top: 50px;">
                                Task creation</p>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="task_name">Task name</label>
                                <div class="col-md-5">
                                    <input id="task_name" required name="name" type="text" class="form-control input-md input_bg_color form_field" value="${task.getName()}" >
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label color" for="datetime">Deadline</label>
                                <div class="col-md-5">
                                    <input type="date" class="form-control" style="background-color: #ffffff" name="deadline" id="datetime" value="${task.getDeadline()?datetime?string('yyyy-MM-dd')}" required>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="group">Group</label>
                                <div class="col-md-5">
                                    <select id="group" name="group" class="form-control input-md" >
                                        <#list groups>
                                            <option disabled>Choose a group</option>
                                            <#items as group>
                                                <#if task.getGroup().getId() == group.getId()>
                                                    <option value="${group.getId()}" selected>${group.getName()}</option>
                                                <#else>
                                                    <option value="${group.getId()}">${group.getName()}</option>
                                                </#if>
                                            </#items>
                                        <#else>
                                            <option disabled>You have no own groups now.</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="description">Description</label>
                                <div class="col-md-5">
                                    <textarea name="description" id="description" class="form-control input-md"><#if task.getDescription()??>${task.getDescription()}</#if></textarea>
                                </div>
                            </div>

                            <input type="submit" value="Edit" class="btn btn-success btn-block btn_black" style="margin-top: 40px;width : 35%; margin-left: 30%;margin-right: 30%;padding-left: 0px;padding-right: 0px;text-align: middle;" />
                        </fieldset>
                    </form >
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>
<@display_page/>