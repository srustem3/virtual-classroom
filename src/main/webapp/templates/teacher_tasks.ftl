<#include "base_teacher.ftl">
<#macro is_tasks> class="active"</#macro>
<#macro content>
<div class="container" id="container_body">
    <div class="jumbotron" id="jumbotron_container" style="padding-right: 15px;padding-left: 15px;">
        <#if message??><h2>${message}</h2></#if>
        <div class="row" style="margin-top: 50px">
            <form role="form" style="width: 100%" class="form-inline">
                <div class="form-group col-sm-2 col-xs-4 col-md-2" style="padding-left: 20px">
                    <h4 style="margin-top: 9px">Search for task</h4>
                </div>
                <div class=" form-group col-sm-3 col-xs-8 col-md-3 margin_mark" style="margin-top: 9px;">
                    <div class="row">
                        <div class="col-sm-4 col-xs-4">
                            <h4 style="margin-top: 0px">Group</h4>
                        </div>
                        <div class="col-sm-7 col-xs-8">
                            <select name="groupFilter" title="group" class="select_">
                                <option value="" selected>Select a group</option>
                                <#list groups>
                                    <#items as group>
                                        <option value="${group.getId()}"<#if groupPar == group.getId()?string> selected</#if>>${group.getName()}</option>
                                    </#items>
                                </#list>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-3 col-xs-6 col-md-3">
                    <input type="text" class="form-control" style="width: 100%" name="nameFilter" id="task_name"<#if name??> value="${name}"</#if> />
                </div>
                <div class="form-group col-sm-2 col-xs-6 col-md-2">
                    <input type="submit" class=" btn btn-default btn_black form-control" style="width: 100%" value="Search" />
                </div>
                <div class="form-group col-sm-2 col-xs-12 col-md-2">
                    <a href="/create_task" name="create" class="btn btn-default btn_black form-control" style="width: 100%">Create</a>
                </div>
            </form>
        </div>
    <#list tasks>
        <table class="table table-hover table-stripped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Date</th>
                <th>Deadline</th>
                <th>Group</th>
            </tr>
            </thead>
            <tbody>
        <#items as task>
            <tr>
                <td>
                    <a href="/tasks/${task.getId()}">${task.getName()}</a>
                </td>
                <td>
                    ${task.getDateTime()?datetime?string('dd.MM.yyyy')}
                </td>
                <td>
                    ${task.getDeadline()?datetime?string('dd.MM.yyyy')}
                </td>
                <td>
                    ${task.getGroup().getName()}
                </td>
            </tr>
        </#items>
            </tbody>
        </table>
    <#else>
    You have no own tasks for students.
    </#list>
    </div>
</div>
</#macro>
<@display_page/>