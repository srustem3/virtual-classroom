<#include "base_student.ftl">
<#macro is_teachers> class="active"</#macro>
<#macro css>
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/css_app.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro styles>
.black_color{
background-color:#699BF7;
color: #ffffff;
}
</#macro>
<#macro content>
<div class="container" id="container_body">
    <div class="jumbotron" id="jumbotron_container" style="padding-right: 15px;padding-left: 15px;">
        <#if message??><h2>${message}</h2></#if>
        <div class="row" style="margin-top: 50px">
            <form role="form" style="width: 100%" class="form-inline">
                <div class="col-sm-3" style="padding-left: 20px">
                    <h4 style="margin-top: 4px">Search for teacher</h4>
                </div>
                <div class="form-group col-sm-7">
                    <input type="text" class="form-control" style="width: 100%" name="teacher_filter" id="teacher_name"<#if login??> value="${login}"</#if>>
                </div>
                <div class="form-group col-sm-2">
                    <input type="submit" class="btn btn-default btn_black form-control" style="width: 100%" value="Search">
                </div>
            </form>
        </div>
        <#list teachers>
            <table id="row" class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>FIO</th>
                    <th>Add teacher</th>
                </tr>
                </thead>
                <tbody id="studentsTable">
                    <#items as teacher>
                        <#if teacher.getId() != user.getId()>
                        <tr>
                            <td>${teacher.getLogin()}</td>
                            <td></td>
                            <td>
                                <button data-toggle="modal" data-target="#addModal" data-tlogin="${teacher.getLogin()}"
                                        class="btn btn-default black_color">
                                    Add
                                </button>
                            </td>
                        </tr>
                        </#if>
                    </#items>
                </tbody>
            </table>
        </#list>
    </div>
</div>
<!-- Modal -->
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Creating application</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="tlogin">Teacher's login</label>
                        <input type="text" id="tlogin" name="name" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="message">Your message</label>
                        <textarea name="text" id="message" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-default btn_color" value="Add"/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#addModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var login = button.data('tlogin');
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find('#tlogin').val(login);
        })
    });
</script>
</#macro>
<@display_page/>