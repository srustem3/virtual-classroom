<#include "base_teacher.ftl">
<#macro is_applications> class="active"</#macro>
<#macro content>
<div class="container" id="container_body">
    <div class="jumbotron" id="jumbotron_container" style="padding-right: 15px;padding-left: 15px;">
        <#if message??><h2>${message}</h2></#if>
            <div class="row" style="margin-top: 50px">
                <form role="form" style="width: 100%" class="form-inline">
                    <div class="col-sm-3" style="padding-left: 20px">
                        <h4 style="margin-top: 4px" >Search for applicant</h4>
                    </div>
                    <div class="form-group col-sm-7">
                        <input type="text" class="form-control" style="width: 100%" name="login_filter" id="teacher_name"<#if login??> value="${login}"</#if>>
                    </div>
                    <div class="form-group col-sm-2">
                        <input type="submit" class="btn btn-default btn_black form-control" style="width: 100%" value="Search">
                    </div>
                </form>
            </div>
        <#list applications>
            <table class="table table-hover table-striped" id="row">
                <thead>
                <tr>
                    <th>FIO</th>
                    <th>Accept/reject student</th>
                </tr>
                </thead>
                <tbody id="studentsTable">
                    <#items as application>
                    <tr>
                        <form method="post">
                            <td>${application.getApplicant().getLogin()}</td>
                            <td>
                                <button type="button" class="btn btn-default btn_color black_color" data-toggle="modal"
                                        data-target="#applyModal"
                                        data-id="${application.getApplicant().getId()}"
                                        data-name="${application.getApplicant().getName()}"
                                        data-message="${application.getText()}">Accept
                                </button>
                                <input type="number" name="student" value="${application.getApplicant().getId()}"
                                       hidden>
                                <input type="submit" name="option" class="btn btn-default" value="reject">
                            </td>
                        </form>
                    </tr>
                    </#items>
                </tbody>
            </table>
        <#else>
            <h2>You have no applications now!</h2>
        </#list>
    </div>
</div>
<!-- Modal -->
<div id="applyModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Accepting</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="number" id="stud_id" name="student" hidden/>
                    <input type="text" name="option" value="accept" hidden/>
                    <p><b>Student's name:</b></p>
                    <p id="stud_name">Name</p>
                    <p><b>Student's message</b></p>
                    <p id="stud_message">Message</p>
                    <div class="form-group">
                        <label for="stud_group">Example select</label>
                        <select class="form-control" id="stud_group" name="group" required>
                            <#list groups>
                                <option disabled selected>Choose a group</option>
                                <#items as group>
                                    <option value="${group.getId()}">${group.getName()}</option>
                                </#items>
                            <#else>
                                <option disabled>You have no own groups now.</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-default btn_color" value="Accept" />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#applyModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id'); // Extract info from data-* attributes
            var name = button.data('name');
            var message = button.data('message');
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find('#stud_id').val(id);
            modal.find('#stud_name').html(name);
            modal.find('#stud_message').html(message);
        })
    });
</script>
</#macro>
<@display_page/>