<#include "base.ftl">
<#macro styles>
@media screen and (max-width: 285px){
.jumbotron p{
font-size: 12px;
}
}

@media screen and (max-width: 260px){
.btn{
font-size: 14px;
}
}

#start_header{
float: right;
margin-right: 50px;
margin-top: 70px;
}


#jumbotron_jumbotron{
padding: 16px ;
margin-right: 100px;
margin-left: 100px;
background-color: #7fadb7;
}

</#macro>
<#macro css>
<link rel="stylesheet" href="/css/css_reg.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro body><body data-spy="scroll" data-target=".navbar" data-offset="50"></#macro>
<#macro navigation>
<div class="nav navbar-nav navbar-right" style="margin-right: 0px">
    <li><a href="/login">Sign in</a></li>
</div>
</#macro>
<#macro content>
<div class="container" style="margin-top: 50px; background-color: #ffffff;">
    <div class="jumbotron" style="padding: 0px;margin-bottom: 0px;">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap" >
                    <form class="form-horizontal login" style="margin-bottom: 50px;width: 80%" method="post">
                        <fieldset class="fieldset">
                            <p class="form-title" style="margin-top: 50px;">
                                Registration</p>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label"  for="email">Email address</label>
                                <div class="col-md-5">
                                    <input id="email" required name="email" type="email" placeholder="Email address" class="form-control input-md input_bg_color form_field" />
                                </div>
                            </div>

                            <div class="form-group form_group_margin" >
                                <label class="col-md-4 control-label"  for="login">Login</label>
                                <div class="col-md-5">
                                    <input id="login" required name="login" type="text" placeholder="Login" class="form-control input_bg_color input-md form_field" />
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label"  for="name">Name</label>
                                <div class="col-md-5">
                                    <input id="name" pattern="^[a-zA-zа-яА-Я]+$" required name="name" type="text" placeholder="Name" class="form-control input-md input_bg_color form_field" />
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label"  for="surname">Surname</label>
                                <div class="col-md-5">
                                    <input id="surname" pattern="^[a-zA-zа-яА-Я]+$" required name="surname" type="text" placeholder="Surname" class="form-control input-md input_bg_color form_field" />
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label"  for="password">Password</label>
                                <div class="col-md-5">
                                    <input id="password" required name="password" type="password" placeholder="Password" class="form-control input-md input_bg_color form_field" />
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label"  for="rpassword">Repeat password</label>
                                <div class="col-md-5">
                                    <input id="rpassword" required name="rpassword" type="password" placeholder="Repeat password" class="form-control input-md input_bg_color form_field" />
                                </div>
                            </div>

                            <input type="submit" id="submit" value="Registration" class="btn btn-default btn-block btn_black" style="  margin-top: 40px;width : 45%; margin-left: 30%;margin-right: 30%;padding-left: 0px;padding-right: 0px;text-align: middle;" />

                        </fieldset>
                    </form >
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#rpassword').change(function() {
            var pass = $("#password").val();
            var pass_rep = $("#rpassword").val();

            if (pass !== pass_rep) {
                $("#rpassword").css('border', 'red 1px solid');
                alert("Passwords do not match.");
            }
        });
    });
</script>
</#macro>
<@display_page/>