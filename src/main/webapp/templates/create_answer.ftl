<#include "base_student.ftl">
<#macro is_answers> class="active"</#macro>
<#macro css>
    <link rel="stylesheet" type="text/css" href="/css/css_reg.css">
    <link rel="stylesheet" type="text/css" href="/css/css_group_create.css">
    <link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro content>
<div class="container" id="container_body">
    <div class="jumbotron" id="jumbotron_container" style="padding-right: 15px;padding-left: 15px;">
        <#if message??><h2>${message}</h2></#if>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap">
                    <form class="form-horizontal login">
                        <fieldset class="fieldset">
                            <p class="form-title color" style="margin-top: 50px;">Answering</p>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label" for="name">Task name</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <label class="input-md control-label color" id="name">${task.getName()}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label color" " for="datetime">Creation
                                date</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color"
                                           id="datetime">${task.getDateTime()?datetime?string('dd.MM.yyyy')}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label color" "
                                for="datetime">Deadline</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color"
                                           id="datetime">${task.getDeadline()?datetime?string('dd.MM.yyyy')}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="description">Description</label>
                                <div class="col-md-5">
                                    <textarea readonly style="background-color: #ffffff" name="description"
                                              id="description" class="form-control input-md"><#if task.getDescription??>${task.getDescription()}<#else>No</#if>
                                    </textarea>
                                </div>
                            </div>
                    </form>
                    <form method="post">
                        <div class="form-group form_group_margin">
                            <label class="col-md-4 control-label" for="decision">Answer</label>
                            <div class="col-md-5">
                                <textarea name="text" id="decision" class="form-control input-md"><#if answer??>${answer.getText()}</#if></textarea>
                            </div>
                        </div>

                        <input type="submit" value="Send" class="btn btn-success btn-block btn_black"
                               style="margin-top: 40px;width : 35%; margin-left: 30%;margin-right: 30%;padding-left: 0px;padding-right: 0px;text-align: middle;"/>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>
<@display_page/>