<#include "base_teacher.ftl">
<#macro is_groups> class="active"</#macro>
<#macro styles>
@media screen and (max-width: 589px){
.btn_black{
font-size: 12px;
}
}
@media screen and (max-width: 410px){
.btn_black{
font-size: 8px;
}
}


table {
max-width:980px;
table-layout:fixed;
margin:auto;
}

thead, tfoot {

display:table;
width:100%;

}
tbody {
height:300px;
overflow:auto;
overflow-x:hidden;
display:block;
width:100%;
}
tbody tr {
display:table;
width:100%;
table-layout:fixed;
}
</#macro>
<#macro css>
<link rel="stylesheet" type="text/css" href="/css/css_reg.css">
<link rel="stylesheet" type="text/css" href="/css/css_group_create.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro content>
<div class="container container_body">
    <div class="jumbotron">
        <#if message??><h2>${message}</h2></#if>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap">
                    <form class="form-horizontal login">
                        <fieldset class="fieldset">
                            <p class="form-title color" style="margin-top: 50px;">
                                Group profile</p>

                            <div class="form-group form_group_margin" style="margin-left: 23%">
                                <label class="col-md-4 control-label" for="group_name">Group name</label>
                                <label id="group_name" class=" col-md-2 control-label"
                                       style="text-align: left;">${group.getName()}</label>
                            </div>


                            <div class="form-group" style="margin-top: 3%; ">
                                <input type="submit" name="delete" value="Delete"
                                       style="margin-top: 20px;width: 20%; margin-left:23%; text-align: center; "
                                       class="col-md-4 col-sm-2 btn btn-default btn_black">

                                <button type="button"
                                        style=" width:20%;margin-top: 20px;text-align: center;margin-left: 10%;"
                                        name="students_list" class="col-md-4 col-sm-3 btn btn-default  btn_black"
                                        data-toggle="modal" data-target="#myModal">Edit
                                </button>
                            </div>

                            <div>
                                <#list studentList>
                                    <table class="table table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th style="padding-right: 9%">Login</th>
                                            <th>Delete student</th>
                                        </tr>
                                        </thead>
                                        <tbody id="studentsTable">
                                            <#items as student>
                                            <tr>
                                                <td>${student.getLogin()}</td>
                                                <td><button type="submit" name="delete_student" value="${student.getId()}"
                                                            class="btn btn-default black_color">Delete</button></td>
                                            </tr>
                                            </#items>
                                        </tbody>
                                    </table>
                                <#else>
                                    <center><h2>The group has no students.</h2></center>
                                </#list>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Group editing</h4>
            </div>
            <form action="/group/edit/${group.getId()}" method="post" style="width: 100%" class="form-horizontal">
                <fieldset class="fieldset">
                    <div class="modal-body">
                        <div class="form-group form_group_margin">
                            <label class="col-md-4 control-label" style="color:#0C1F42 " for="group_name">Group name</label>
                            <div class="col-md-5">
                                <input id="group_name" required name="name" type="text" placeholder="Group name" class="form-control input-md input_bg_color form_field" style="color:#0C1F42 ;" value="${group.getName()}" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <input type="submit" value="Edit" class="btn btn-default btn-block col-md-offset-5 col-sm-offset-5 col-xs-offset-5 col-sm-3 col-md-3 col-xs-3" style="margin-top:5px;background-color: #0C1F42;color: #ffffff;width: 20%;text-align: center;padding-right: 0px;padding-left: 0px;" />
                            <button type="button" class="btn btn-default btn-block col-sm-3 col-md-3 col-xs-3" style="width: 20%" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </fieldset>
            </form >
        </div>

    </div>
</div>
</#macro>
<@display_page/>