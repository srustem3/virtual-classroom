<#include "base_student.ftl">
<#macro is_answers> class="active"</#macro>
<#macro styles>
@media screen and (max-width: 350px){
h4{
font-size: 12px;
}
.btn-black{
font-size: 10px;
}
}
</#macro>
<#macro content>
<div class="container" id="container_body">
    <div class="jumbotron" id="jumbotron_container" style="padding-right: 15px;padding-left: 15px;">
        <#if message??><h2>${message}</h2></#if>
        <div class="row" style="margin-top: 50px">
            <form role="form" style="width: 100%" class="form-inline">
                <div class="form-group col-sm-2 col-xs-4 col-md-2" style="padding-left: 20px">
                    <h4 style="margin-top: 9px">Search for answer</h4>
                </div>
                <div class="form-group col-sm-3 col-xs-8 col-md-3 margin_mark" style="margin-top: 9px;">
                    <div class="row">
                        <div class="col-sm-5 col-xs-4">
                            <h4 style="margin-top: 0px">Group</h4>
                        </div>
                        <div class="col-sm-7 col-xs-8">
                            <select title="group" name="group_filter" class="select_">
                                <option value="">Select a group</option>
                                <#list groups>
                                    <#items as group>
                                        <option value="${group.getId()}"<#if groupPar == group.getId()?string> selected</#if>>${group.getName()}</option>
                                    </#items>
                                </#list>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-3 col-xs-6 col-md-5">
                    <input type="text" class="form-control" style="width: 100%" name="name_filter" id="task_name"<#if name??> value="${name}"</#if>>
                </div>
                <div class="form-group col-sm-2 col-xs-6 col-md-2">
                    <input type="submit" class=" btn btn-default btn_black form-control">
                </div>
            </form>
        </div>
        <#list answers>
        <table class="table table-hover table-stiped" id="row">
            <thead>
            <tr>
                <th>Name</th>
                <th>Deadline</th>
                <th>Teacher</th>
                <th>Group</th>
                <th>Mark</th>
            </tr>
            </thead>
            <tbody>
            <#items as answer>
            <tr>
                <td><a href="/tasks/do/${answer.getTask().getId()}">${answer.getTask().getName()}</a>
                </td>
                <td>
                    ${answer.getTask().getDeadline()?datetime?string('dd.MM.yyyy')}
                </td>
                <td>
                    ${answer.getTask().getGroup().getTeacher().getLogin()}
                </td>
                <td>
                    ${answer.getTask().getGroup().getName()}
                </td>
                <td>
                    <#if answer.getScore()??>${answer.getScore()}<#else>-</#if>
                </td>
            </tr>
            </#items>
            </tbody>
        </table>
        <#else>
        You have no own answers.
        </#list>
    </div>
</div>
</#macro>
<@display_page/>