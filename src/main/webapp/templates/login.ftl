<#include "base.ftl">
<#macro styles>
@media screen and (max-width: 670px){
form.login{
width: 65%;
}
}

form{
width: 45%;
}

p.form-title{
color: #0C1F42;
}

.text_color{
color: #eeeeee;
}


.jumbotron h1 {
color: #0C1F42;
}

body {
position: relative;
}
</#macro>
<#macro body><body data-spy="scroll" data-target=".navbar" data-offset="50"></#macro>
<#macro navigation>
<div class="nav navbar-nav navbar-right" style="margin-right: 0px">
    <li><a href="/register">Registration</a></li>
</div>
</#macro>
<#macro content>
<div class="container b_con" style="margin-top: 50px;height: 700px">
    <div class="jumbotron" style="padding: 0px;height: 100%">
        <div class="container-fluid" style=" background-size: 100% 100%;">
            <div class="container sec_con">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap">
                            <p class="form-title text_color" style="margin-top: 10%">
                                Log In</p>
                            <#if message??><center><h2>${message}</h2></center></#if>
                            <form class="login" method="post">
                                <input type="text" name="login" placeholder="Login/E-mail" class="text_color"
                                       style="background-color: #ffffff;color:#222;margin-top: 4%;"/>
                                <input type="password" name="password" placeholder="Password" class="text_color"
                                       style="background-color: #ffffff;color:#222;margin-top: 4%;"/>
                                <input type="submit" value="Log In" class="btn btn-default btn-sm btn_black"
                                       style="margin-top: 4%"/>
                                <div class="remember-forgot">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label class="text_color" style="color: #0C1F42;">
                                                    <input type="checkbox" name="rememberMe" value="rememberMe"/>
                                                    Remember Me
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>
<@display_page/>