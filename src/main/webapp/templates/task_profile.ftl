<#include "base_teacher.ftl">
<#macro is_tasks> class="active"</#macro>
<#macro css>
<link rel="stylesheet" type="text/css" href="/css/css_reg.css">
<link rel="stylesheet" type="text/css" href="/css/css_group_create.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro styles>
.black_color{
background-color: #699BF7;
color: #ffffff;
text-align: center;
}


@media screen and (max-width: 589px){
.black_color{
font-size: 12px;
}
}
@media screen and (max-width: 410px){
.black_color{
font-size: 8px;
}
}

</#macro>
<#macro content>
<div class="container container_body">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap" >
                    <form class="form-horizontal login">
                        <fieldset class="fieldset">
                            <p class="form-title color" style="margin-top: 50px;">
                                Task profile</p>
                            <#if message??><h2>${message}</h2></#if>
                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label" for="task_name">Task name</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color" id="task_name">${task.getName()}</label>
                                </div>
                            </div>


                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label" for="student_name">Creation date</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <label class="input-md control-label color" id="student_name">${task.getDateTime()?datetime?string('dd.MM.yyyy')}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label color" for="datetime">Deadline</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color" id="datetime">${task.getDeadline()?datetime?string('dd.MM.yyyy')}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label color" for="group">Group</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color" id="group">${task.getGroup().getName()}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label" for="description">Description</label>
                                <div class="col-md-5 ">
                                    <textarea readonly name="description" style="background-color: #ffffff" id="description" class="form-control input-md"><#if task.getDescription()??>${task.getDescription()}</#if></textarea>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 3%; ">
                                <input type="submit" name="delete" value="Delete"
                                       style="margin-top: 20px;width: 20%; margin-left:23%; text-align: center; "
                                       class="col-md-4 col-sm-2 btn btn-default btn_black">

                                <a href="/task/edit/${task.getId()}" type="button"
                                        style=" width:20%;margin-top: 20px;text-align: center;margin-left: 10%;"
                                        name="students_list" class="col-md-4 col-sm-3 btn btn-default  btn_black">Edit</a>
                            </div>

                        </fieldset>
                    </form>
                    <#list answers>
                    <table id="row" class="table table-hover table-striped" >
                        <thead>
                        <tr>
                            <th>Student</th>
                            <th>Show decision</th>
                        </tr>
                        </thead>
                        <tbody id="studentsTable">
                        <#items as answer>
                        <tr>
                            <td>${answer.getUser().getStudent().getLogin()}<#if answer.getScore()??> (score: ${answer.getScore()})</#if></td>
                            <td><a href="/answers_evaluate/${answer.getId()}" class="btn btn-default black_color" type="submit">Show decision</a></td>
                        </tr>
                        </#items>
                        </tbody>
                    </table>
                    <#else>
                    This task has no answers.
                    </#list>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>
<@display_page/>