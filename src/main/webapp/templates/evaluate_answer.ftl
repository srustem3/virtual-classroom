<#include "base_teacher.ftl">
<#macro is_tasks> class="active"</#macro>
<#macro css>
<link rel="stylesheet" type="text/css" href="/css/css_reg.css">
<link rel="stylesheet" type="text/css" href="/css/css_group_create.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro styles>
.btn_black{
text-align: center;
}


@media screen and (max-width: 589px){
.btn_black{
font-size: 12px;
}
}
@media screen and (max-width: 410px){
.btn_black{
font-size: 8px;
}
}
</#macro>
<#macro content>
<div class="container container_body">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap">
                    <form class="form-horizontal login">
                        <fieldset class="fieldset">
                            <p class="form-title color" style="margin-top: 50px;">
                                Task decision</p>
                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label" for="task_name">Task
                                    name</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color"
                                           id="task_name">${answer.task.getName()}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label" for="student_name">Student
                                    name</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <label class="input-md control-label color"
                                           id="student_name">${answer.getUser().getStudent().getLogin()}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label color" "
                                for="datetime">Deadline</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color"
                                           id="datetime">${answer.task.getDeadline()}</label>
                                </div>
                            </div>


                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="description">Description</label>
                                <div class="col-md-5 ">
                                    <textarea readonly name="description" style="background-color: #ffffff"
                                              id="description" class="form-control input-md"><#if answer.task.getDescription()??>${answer.task.getDescription()}</#if></textarea>

                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="decision">Decision</label>
                                <div class="col-md-5 ">
                                    <textarea readonly name="decision" style="background-color: #ffffff" id="decision"
                                              class="form-control input-md">${answer.getText()}</textarea>

                                </div>
                            </div>
                    </form>
                    <form method="post" class="form-horizontal login">

                        <div class="form-group form_group_margin">
                            <label class="col-md-4 control-label" for="mark">Mark</label>
                            <div class="col-md-5 ">
                                <input type="number" step="0.01" min="0" name="mark" style="background-color: #ffffff"
                                       id="mark" class="form-control input-md" <#if answer.getScore()??>value="${answer.getScore()}"</#if> required>
                            </div>
                        </div>

                        <div class="form-group form_group_margin">
                            <label class="col-md-4 control-label" for="decision">Comment</label>
                            <div class="col-md-5 ">
                                <textarea name="comment" style="background-color: #ffffff" id="comment"
                                          class="form-control input-md"><#if answer.getComment()??>${answer.getComment()}</#if></textarea>

                            </div>
                        </div>

                        <input name="delete" type="submit" class=" form-control btn btn-default btn_black"
                               value="Submit">

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>
<@display_page/>