<#include "base_teacher.ftl">
<#macro is_groups> class="active"</#macro>
<#macro styles>
    .btn_black{
        text-align: center;
    }

    @media screen and (max-width: 350px){
        h4{
            font-size: 12px;
        }
        .btn_black{
            font-size: 8px;
        }
    }
</#macro>
<#macro content>
<div class="container" id="container_body">
    <div class="jumbotron" id="jumbotron_container" style="padding-right: 15px;padding-left: 15px;">
        <#if message??><h2>${message}</h2></#if>
        <div class="row" style="margin-top: 50px">
            <form role="form" style="width: 100%" class="form-inline">
                <div class="col-sm-2" style="padding-left: 20px">
                    <h4 style="margin-top: 4px" >Search for group</h4>
                </div>
                <div class="form-group col-sm-6">
                    <input type="text" class="form-control" style="width: 100%" name="group_name" id="group_name" <#if name??>value="${name}"</#if>>
                </div>
                <div class="form-group col-sm-2">
                    <input type="submit" class="btn btn-default btn_black form-control" style="width: 100%" value="Search">
                </div>
                <div class="form-group col-sm-2">
                    <button type="button" name="create" class="btn btn-default btn_black form-control" style="width: 100%" data-toggle="modal" data-target="#myModal">Create group</button>
                </div>
            </form>
        </div>
        <#list groups>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Group</th>
            </tr>
            </thead>
            <tbody>
            <#items as group>
            <tr>
                <td>
                    <a href="/groups/${group.getId()}">
                        ${group.getName()}
                    </a>
                </td>
            </tr>
            </#items>
            </tbody>
        </table>
        <#else>
        <h1>You have no own groups.</h1>
        </#list>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Group creation</h4>
            </div>
            <form style="width: 100%" class="form-horizontal" action="/create_group" method="post">
                <fieldset class="fieldset">
                    <div class="modal-body">

                        <div class="form-group form_group_margin">
                            <label class="col-md-4 control-label" style="color:#0C1F42 " for="group_name">Group name</label>
                            <div class="col-md-5">
                                <input id="group_name" pattern="^([a-z0-9-]+$" required name="name" type="text" placeholder="Group name" class="form-control input-md input_bg_color form_field" style="color:#0C1F42 ;"  >
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <input type="submit" value="Create" class="btn btn-default btn-block col-md-offset-5 col-sm-offset-5 col-xs-offset-5 col-sm-3 col-md-3 col-xs-3" style="margin-top:5px;background-color: #0C1F42;color: #ffffff;width: 20%;text-align: center;padding-right: 0px;padding-left: 0px;" />
                            <button type="button" class="btn btn-default btn-block col-sm-3 col-md-3 col-xs-3" style="width: 20%" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </fieldset>
            </form >
        </div>
    </div>
</div>
</#macro>
<@display_page/>