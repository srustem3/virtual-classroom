<#include "base.ftl">
<#macro is_teachers></#macro>
<#macro is_tasks></#macro>
<#macro is_answers></#macro>
<#macro body><body id="body" style="background-color: #ffffff"></#macro>
<#macro navigation>
<ul class="nav navbar-nav">
    <li<@is_teachers/>><a href="/create_application">Teachers</a></li>
    <li<@is_tasks/>><a href="/my_tasks">Undone tasks</a></li>
    <li<@is_answers/>><a href="/my_answers">Answers</a></li>
</ul>
<div class="nav navbar-nav navbar-right" style="margin-right: 0px">
    <div class="dropdown" style="margin-top: 10px">
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Exit/Switch
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu">
            <li><a href="/logout">Exit</a></li>
            <li><a href="/groups">Switch role</a></li>
        </ul>
    </div>
</div>
</#macro>