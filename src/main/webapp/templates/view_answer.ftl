<#include "base_student.ftl">
<#macro is_answers> class="active"</#macro>
<#macro styles>
.btn_black{

text-align: center;
}


@media screen and (max-width: 589px){
.btn_black{
font-size: 12px;
}
}
@media screen and (max-width: 410px){
.btn_black{
font-size: 8px;
}
}
</#macro>
<#macro css>
<link rel="stylesheet" type="text/css" href="/css/css_reg.css">
<link rel="stylesheet" type="text/css" href="/css/css_group_create.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro content>
<div class="container container_body">
    <div class="jumbotron">
        <#if message??><h2>${message}</h2></#if>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="wrap" >
                    <form class="form-horizontal login">
                        <fieldset class="fieldset">
                            <p class="form-title color" style="margin-top: 50px;">
                                View answer</p>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4  control-label" for="task_name">Task name</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <label class="input-md control-label color" id="task_name">${answer.task.getName()}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label" for="student_name">Student login</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color"  id="student_name">${answer.getUser().getStudent().getLogin()}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin" >
                                <label class="col-md-4 col-sm-4 col-xs-4 control-label color" " for="datetime">Deadline</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <label class="input-md control-label color" id="datetime">${answer.task.getDeadline()?datetime?string('dd.MM.yyyy')}</label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="description">Description</label>
                                <div class="col-md-5">
                                    <textarea readonly style="background-color: #ffffff" name="description" id="description" class="form-control input-md"><#if answer.task.getDescription()??>${answer.task.getDescription()}<#else>No</#if></textarea>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="decision">Answer</label>
                                <div class="col-md-5">
                                    <textarea readonly style="background-color: #ffffff" name="decision" class="form-control input-md">${answer.getText()}</textarea>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="student_name">Mark</label>
                                <div class="col-md-5">
                                    <label class="input-md control-label color"  id="student_name"><#if answer.getScore()??>${answer.getScore()}<#else>No</#if></label>
                                </div>
                            </div>

                            <div class="form-group form_group_margin">
                                <label class="col-md-4 control-label" for="decision">Teacher's comment</label>
                                <div class="col-md-5">
                                    <textarea readonly style="background-color: #ffffff" name="decision" class="form-control input-md"><#if answer.getComment()??>${answer.getComment()}<#else>No</#if></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </form >
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>
<@display_page/>