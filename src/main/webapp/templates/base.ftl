<#macro styles></#macro>
<#macro navigation></#macro>
<#macro content></#macro>
<#macro body></#macro>
<#macro css>
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="/css/css_app.css">
<link rel="stylesheet" type="text/css" href="/css/black_btn.css">
</#macro>
<#macro display_page>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <@css/>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>


    <style>
        <@styles/>
    </style>
</head>
<@body/>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/login">STUDTEACH</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
                <@navigation/>
        </div>
    </div>
</nav>
    <@content/>
</body>
</html>
</#macro>