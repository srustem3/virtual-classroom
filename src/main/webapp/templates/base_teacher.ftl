<#include "base.ftl">
<#macro is_groups></#macro>
<#macro is_tasks></#macro>
<#macro is_applications></#macro>
<#macro body><body id="body" style="background-color: #ffffff"></#macro>
<#macro navigation>
<ul class="nav navbar-nav">
    <li<@is_groups/>><a href="/groups">Groups</a></li>
    <li<@is_tasks/>><a href="/teacher_tasks">Tasks</a></li>
    <li<@is_applications/>><a href="/applicants">Applications</a></li>
</ul>
<div class="nav navbar-nav navbar-right" style="margin-right: 0px">
    <div class="dropdown" style="margin-top: 10px">
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Exit/Switch
            <span class="caret"></span>
        </button>

        <ul class="dropdown-menu">
            <li><a href="/logout">Exit</a></li>
            <li><a href="/my_tasks">Switch role</a></li>
        </ul>
    </div>
</div>
</#macro>