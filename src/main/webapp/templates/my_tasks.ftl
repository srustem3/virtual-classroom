<#include "base_student.ftl">
<#macro is_tasks> class="active"</#macro>
<#macro styles>
@media screen and (max-width: 350px){
h4{
font-size: 12px;
}
.btn-black{
font-size: 10px;
}
}
</#macro>
<#macro content>
<div class="container" id="container_body">
    <div class="jumbotron" id="jumbotron_container" style="padding-right: 15px;padding-left: 15px;">
        <#if message??><h2>${message}</h2></#if>
        <div class="row" style="margin-top: 50px">
            <form role="form" style="width: 100%" class="form-inline">
                <div class="form-group col-sm-2 col-xs-4 col-md-2" style="padding-left: 20px">
                    <h4 style="margin-top: 9px">Search for task</h4>
                </div>
                <div class="form-group col-sm-3 col-xs-8 col-md-3 margin_mark" style="margin-top: 9px;">
                    <div class="row">
                        <div class="col-sm-5 col-xs-4">
                            <h4 style="margin-top: 0px">Group</h4>
                        </div>
                        <div class="col-sm-7 col-xs-8">
                            <select title="group" name="group_filter" class="select_">
                                <option value="" selected>Select a group</option>
                                <#list groups>
                                    <#items as group>
                                        <option value="${group.getId()}"<#if groupPar == group.getId()?string> selected</#if>>${group.getName()}</option>
                                    </#items>
                                </#list>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-3 col-xs-6 col-md-5">
                    <input type="text" class="form-control" style="width: 100%" name="name_filter" id="task_name"<#if name??> value="${name}"</#if>>
                </div>
                <div class="form-group col-sm-2 col-xs-6 col-md-2">
                    <input type="submit" class=" btn btn-default btn_black form-control">
                </div>
            </form>

        </div>
        <table class="table table-hover table-stripped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Date</th>
                <th>Deadline</th>
                <th>Teacher</th>
                <th>Group</th>
            </tr>
            </thead>
            <tbody>
                <#list taskList>
                    <#items as task>
                    <tr>
                        <td>
                            <a href="/tasks/do/${task.getId()}">${task.getName()}</a>
                        </td>
                        <td>
                        ${task.getDateTime()?datetime?string('dd.MM.yyyy')}
                        </td>
                        <td>
                        ${task.getDeadline()?datetime?string('dd.MM.yyyy')}
                        </td>
                        <td>
                        ${task.getGroup().getTeacher().getLogin()}
                        </td>
                        <td>
                            ${task.getGroup().getName()}
                        </td>
                    </tr>
                    </#items>
                </#list>
            </tbody>
        </table>
    </div>
</div>
</#macro>
<@display_page/>