Name: ${answer.task.getName()}<br>
Deadline: ${answer.task.getDeadline()}<br>
Description: <#if answer.task.getDescription()??>${answer.task.getDescription()}</#if><br>
Answer: ${answer.getText()}<br>
Student: ${answer.getUser().getLogin()}<br>
<form method="post">
    <label for="mark">Mark:</label>
    <input type="number" id="mark" name="mark" step="0.01" min="0" <#if answer.getScore()??>value="${answer.getScore()}"</#if> required><br>
    <label for="comment">Комментарий к решению:</label>
    <textarea id="comment" name="comment"><#if answer.getComment()??>${answer.getComment()}</#if></textarea><br>
    <input type="submit" value="Оценить">
</form>
