<#if message??><h1>${message}</h1></#if>
<#list applications>
<form action="/applicants" method="post">
    <label for="application">Applications</label>
    <select id="application" name="student" required>
        <#items as application>
            <option value="${application.getApplicant().getId()}">${application.getApplicant().getLogin()} - ${application.getText()}</option>
        </#items>
    </select><br>
    <label for="group">Group</label>
    <select id="group" name="group" required>
        <#list groups>
            <option disabled>Choose a group</option>
            <#items as group>
                <option value="${group.getId()}">${group.getName()}</option>
            </#items>
        <#else>
            <option disabled>You have no own groups now.</option>
        </#list>
    </select><br>
    <label for="option">Option</label>
    <select id="option" name="option" required>
        <option disabled>Choose an option</option>
        <option value="accept">Accept</option>
        <option value="reject">Reject</option>
    </select><br>
    <input type="submit" value="Process application">
</form>
<#else>
    <p>You have no applications.</p>
</#list>