Name: ${answer.task.getName()}<br>
Deadline: ${answer.task.getDeadline()}<br>
Description: <#if answer.task.getDescription()??>${answer.task.getDescription()}<#else>No</#if><br>
Answer: ${answer.getText()}<br>
Student: ${answer.getUser().getLogin()}<br>
Mark: <#if answer.getScore()??>${answer.getScore()}<#else>No</#if><br>
Comment: <#if answer.getComment()??>${answer.getComment()}<#else>No</#if>